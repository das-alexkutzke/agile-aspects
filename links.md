Especialização em Desenvolvimento Ágil de Software

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Aspectos Ágeis de Programação*

Prof. Alexander Robert Kutzke

# Links interessantes

## Projetos "bem organizados" (ReactJS)

- [Monorepo that holds all of HospitalRun's v2 projects.](https://github.com/HospitalRun/hospitalrun);
- [A simplified Jira clone built with React/Babel (Client), and Node/TypeScript (API)](https://github.com/oldboyxx/jira_clone);
- [Clean architecture based react project sample code](https://github.com/falsy/react-with-clean-architecture);
- [Examples of large production-grade, open-source React apps](https://maxrozen.com/examples-of-large-production-grade-open-source-react-apps).


## Seleção de links diversos temas

- [Awesome Clean Code](https://github.com/kkisiele/awesome-clean-code);
- [Awesome BDD](https://github.com/omergulen/awesome-bdd);
- [Awesome TDD](https://github.com/omergulen/awesome-tdd);
- [Awesome TDD 2](https://github.com/unicodeveloper/awesome-tdd);
