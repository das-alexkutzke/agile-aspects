% Clean Code: Nomes
% Alex Kutzke
% Agosto 2023

# Nomes são importantes

## Exemplo

```{.js}
class MainEntity {
  process() {}
}

const login = true;

const us = new MainEntity();
us.process();

if (login) {
  // ...
}
``` 

## Melhor?

```{.js}
class User {
  save() {}
}

const isLoggedIn = true;

const user = new User();
user.save();

if (isLoggedIn) {
  // ...
}
```

## Nomes importam

- Ao programar, nós nomeamos coisas o tempo todo
- Arquivos, variáveis, constantes, classes, métodos, funções ...
- Precisamos fazer isso bem!
- Vejamos algumas regras (ou sugestões) simples

# Algumas dicas

## Use nomes que revelam intenção

- Um nome precisa responder as seguintes questões:
  - Por que isso existe?
  - O que isso faz?
  - Como isso é utilizado?

```{.js }
var d:number; // elapsed time in days
```

Que tal?

```{.ts}
var elapsedTimeInDays:number;
var daysSinceCreation:number;
var daysSinceModification:number;
var fileAgeInDays:number;
```

## Use nomes que revelam intenção

### Exemplo ruim

```{.java}
public List<int[]> getThem() {
  List<int[]> list1 = new ArrayList<int[]>();
  for (int[] x : theList)
    if (x[0] == 4)
      list1.add(x);
  return list1;
}
```

## Use nomes que revelam intenção

### Exemplo um pouco melhor

```{.java}
public List<int[]> getFlaggedCells() {
  List<int[]> flaggedCells = new ArrayList<int[]>();
  for (int[] cell : gameBoard)
    if (cell[STATUS_VALUE] == FLAGGED)
      flaggedCells.add(cell);
  return flaggedCells;
}
```

## Evite desinformação

- Evite deixar pistas incorretas ou dúvidas

- `accountList` é mesmo uma lista? Ou apenas uma coleção?
  - `accountGroup` ou `bunchOfAccounts` seria melhor;

- Qual a diferença:
  - `XYZControllerForEfficientHandlingOfStrings`
  - `XYZControllerForEfficientStorageOfStrings`

- **Consistência** é informação

## Evite desinformação

O que há de terrível aqui?

```{.ts}
var a = l;
if ( O == l )
  a = O1;
else
  l = 01;
```

. . . 

- Mais comum do que se imagina :(

## Faça distinções significativas

- Coisas diferentes devem ser claramente diferentes
- Sim, isso quer dizer que você não deve ter nomes assim: `a1, a2, a3 ...`

```{.js}
public static void copyChars(char a1[], char a2[]) {
  for (int i = 0; i < a1.length; i++) {
    a2[i] = a1[i];
  }
}
```
. . . 

  Por que não `source` e `destination`?

## Faça distinções significativas

- Cuidado com palavras redundantes na hora de diferenciar nomes:

```{.js}
ProductInfo
ProductData
```

- Info e Data podem ser palavras de **ruído**:
  - São palavras redundantes em um certo contexto
  - `variable` em um nome de variável, `table` em um nome de tabela ...
  - `NameString` ou apenas `Name`?

```{.js}
// Um exemplo real ...
getActiveAccount();
getActiveAccounts();
getActiveAccountInfo();
```

## Use nomes pronunciáveis

- Somos bons com palavras

> "Se você não consegue pronunciar, então você não conseguirá discutir sem parecer um idiota."

```{.java}
class DtaRcrd102 {
  private Date genymdhms;
  private Date modymdhms;
  private final String pszqint = "102";
  /* … */
};
```

```{.java}
class Customer {
  private Date generationTimestamp;
  private Date modificationTimestamp;
  private final String recordId = "102";
  /* … */
};
```

## Use nomes buscáveis

- Utilize constantes, não números
- Utilize palavras, não letras:
  - Variáveis de uma única letra podem ser utilizar APENAS como variáveis locais dentro de escopos **curtos**

```{.java}
for (int j=0; j<34; j++) {
  s += (t[j]*4)/5;
}
```

```{.java}
int realDaysPerIdealDay = 4;
const int WORK_DAYS_PER_WEEK = 5;
int sum = 0;
for (int j=0; j < NUMBER_OF_TASKS; j++) {
  int realTaskDays = taskEstimate[j] * realDaysPerIdealDay;
  int realTaskWeeks = (realdays / WORK_DAYS_PER_WEEK);
  sum += realTaskWeeks;
}
```

## Evite *encodings*

- Antigamente era mais comum termos que adicionar informações sobre a variável, como tipagem, no seu nome
- Por exemplo, adicionar uma letra `m` no início de nomes de métodos
- Ou `c` em nomes de classes
- Isso não é mais "necessário"

## Evite mapeamento mental

- *"Clarity is king"*
- Não faça o leitor ter que mapear nomes mentalmente
  - Ou seja, dar um nome mental para uma variável enquanto lê o código, por exemplo
- Isso geralmente ocorre com variáveis de uma única letra
  - "Ah, `u` significa `user`!"

## Não seja engraçadinho

- A piada só fará sentido para quem a conhece
- *"Say what you mean. Mean what you say"*

## Escolha uma Palavra por Conceito

- Novamente, consistência
- Utilize uma mesma palavra sempre que nomear um mesmo conceito
- O exemplo clássico: `fetch`, `retrieve` e `get`
- Escolha um e mantenha o padrão
- Nomes diferentes nos levam a esperar comportamentos diferentes

## Não faça trocadilhos

- Por outro lado, não utilize a mesma palavra para conceitos diferentes
- Por exemplo: 
  - Boa parte das suas classes possui o método `add()` que cria um novo valor por adição ou concatenação de dois valores existentes
  - Por "consistência" você decide nomear um novo método de `add()` que adiciona um novo elemento a uma coleção
  - As semânticas são diferentes
  - `insert()` ou `append()` seriam opções melhores

## Use nomes do domínio da solução

- Utilize nomes de significado estabelecido na computação:
  - Nomes de algoritmos
  - Padrões de design
  - Etc.

## Use nomes do domínio do problema

- Quando não houver um bom nome computacional para o que você quer, utilize um nome existente nas regras de negócio
- Assim, quem ler seu código poderá, ao menos, perguntar aos demais o que um certo nome significa

## Insira contexto significativo

- Nomes geralmente precisam de contexto para serem corretamente entendidos
- É possível adicionar prefixos a nomes para dar contexto: `addrState`, `addrNum`, ...
- Porém, pode-se alterar a organização do código para adicionar contexto:
  - Classes (`Address` no exemplo acima)
  - Métodos
  - Funções

- Algo como separar um texto em capítulos e seções. ;)

## Não insira contexto gratuito

- Imagine que você implementou uma aplicação de redes neurais e decidiu iniciar todas as classes com as iniciais RN
- Agora, no seu editor de última geração, digite RN ...
- Ajude a IDE a te ajudar :)

# "Resumo"

## Muitas regras

Não dá pra ser mais prático?

. . . 

Maximilian Schwarzmüller fez um bom resumo para as práticas de nomeação, separando em 3 grupos:

- Variáveis, constantes e propriedades
- Funções em métodos
- Classes

## Variáveis, constantes e propriedades

::: r-fit-text

|                 | Objeto | Número ou String | Booleano |
|-----------------|--------|------------------|----------|
| Regra           | Descreva o valor | Descreva o valor | Responda uma pergunta de verdadeiro ou falso |
| Exemplo         | `user`, `database` | `name`, `age` | `isActive`, `loggedIn` |
| Melhoramento    | Dê o maior nível de detalhamento sem adicionar redundância | Dê o maior nível de detalhamento sem adicionar redundância | Dê o maior nível de detalhamento sem adicionar redundância |
| Exemplo Melhor  | `authenticateUser`, `sqlDatabase` | `firstName`, `age` | `isActiveUser`, `loggedIn` |

:::

## Variáveis, constantes e propriedades

| Qual dado | Nome ruim | Nome ok | Nome bom |
|-----------|-----------|---------|----------|
|Objeto de usuário| `u`, `data` | `userData`, `person` | `user`, `customer` |
|Resultado de validação de entrada (true/false)| `v`, `val` | `correct`, `validatedInput` | `isCorrect`, `isValid` |

## Funções e métodos

::: r-fit-text
|                 | Função que realiza operação | Função que retorna Booleano |
|-----------------|-----------------------------|-----------------------------|
| Regra           | Descreva a operação | Responda uma pergunta de verdadeiro ou falso |
| Exemplo         | `getUser()`, `response.send()` | `isValid()`, `purchase.isPaid()` |
| Melhoramento    | Dê o maior nível de detalhamento sem adicionar redundância | Dê o maior nível de detalhamento sem adicionar redundância |
| Exemplo Melhor  | `getUserByEmail()`, `response.send()` | `emailIsValid()`, `purchase.isPaid()` |
:::


## Funções e métodos

| Qual dado | Nome ruim | Nome ok | Nome bom |
|-----------|-----------|---------|----------|
|Salva dados de usuário no BD| `process()`, `handle()` | `save()`, `storeData()` | `saveUser()`, `user.store()` |
|Valida entrada (true/false)| `process()`, `save()` | `validateSave()`, `check()` | `validate()`, `isValid()` |

## Classes

|                 | Classe                      |
|-----------------|-----------------------------|
| Regra           | Descreva o objeto           | 
| Exemplo         | `User`, `Product` |
| Melhoramento    |  Dê o maior nível de detalhamento sem adicionar redundância |
| Exemplo Melhor  | `Customer`, `Course` |

- Evite sufixos redundantes (`DatabaseManager`):
  - Quando instanciado, deixa de fazer sentido

## Classes

| Qual dado | Nome ruim | Nome ok | Nome bom |
|-----------|-----------|---------|----------|
|Um usuário | `class UEntity`, `class ObjA` | `class UserObj`, `class AppUser` | `class User`, `class Admin` |
|Uma base de dados| `class Data`, `class DataStorage` | `class Db`, `class Data` | `class Database`, `class SQLDatabase` |

## Exceções

```{.python}
from datetime import datetime

class DateUtil:
    @staticmethod
    def get_formatted_today():
        date_today = datetime.now()
        formatted_date = date_today.strftime('%Y-%m-%d')
        return formatted_date


print(DateUtil.get_formatted_today())
```

## Exceções

```{.js .r-stretch }
class Database {
  private client: any;

  get connectedClient() {
    if (!this.client) {
      throw new Error('Database not connected!');
    }
    return this.client;
  }

  connect() {
    // Establishing connection ...
    this.client = {};
  }
}

const db = new Database();
db.connectedClient.query();
```

# Prática

## Prática: como melhorar esse código? 

```{.python .r-stretch }
from datetime import datetime


class Entity:
    def __init__(self, title, description, ymdhm):
        self.title = title
        self.description = description
        self.ymdhm = ymdhm


def output(item):
    print('Title: ' + item.title)
    print('Description: ' + item.description)
    print('Published: ' + item.ymdhm)


summary = 'Clean Code Is Great!'
desc = 'Actually, writing Clean Code can be pretty fun. You\'ll see!'
new_date = datetime.now()
publish = new_date.strftime('%Y-%m-%d %H:%M')

item = Entity(summary, desc, publish)

output(item)
```

## Exercício (apenas para praticar)

```{.python .r-stretch}
class Point:
    def __init__(self, coordX, coordY):
        self.coordX = coordX
        self.coordY = coordY


class Rectangle:
    def __init__(self, starting_point, broad, high):
        self.starting_point = starting_point
        self.broad = broad
        self.high = high

    def area(self):
        return self.broad * self.high

    def end_points(self):
        top_right = self.starting_point.coordX + self.broad
        bottom_left = self.starting_point.coordY + self.high
        print('Starting Point (X)): ' + str(self.starting_point.coordX))
        print('Starting Point (Y)): ' + str(self.starting_point.coordY))
        print('End Point X-Axis (Top Right): ' + str(top_right))
        print('End Point Y-Axis (Bottom Left): ' + str(bottom_left))


def build_stuff():
    main_point = Point(50, 100)
    rect = Rectangle(main_point, 90, 10)

    return rect


my_rect = build_stuff()

print(my_rect.area())
my_rect.end_points()
```

## Referências

- Martin, Robert C. Clean code: a handbook of agile software craftsmanship. Pearson Education, 2009.
- Schwarzmüller, Maximilian. Clean code course. Academind, 2021.

