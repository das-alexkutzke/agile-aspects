Especialização em Desenvolvimento Ágil de Software

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Aspectos Ágeis de Programação*

Prof. Alexander Robert Kutzke

# Code Smells

Termo popularizado por Martin Fowler, "Code Smells" são características identificáveis
em um código fonte que podem indicar problemas mais profundos com a implementação.

Alguns links interessantes:

- [Code Smells - Refactoring Guru](https://refactoring.guru/refactoring/smells);
- [Code Smells em JS](https://github.com/mohuk/js-code-smells);
- [Refatoração no VScode](https://code.visualstudio.com/docs/editor/refactoring);

# Análise estática de código

Análise estática de código é o procedimento de identificação de problemas
ou indicações de problemas e, em alguns casos, sua correção, em um código fonte 
pela única e exclusiva análise do código em si. Ou seja, sem considerar sua compilação
e/ou execução.

- [JSlint](https://www.jslint.com/);
- [ESLint](https://eslint.org/);
- [SonarCloud](https://sonarcloud.io/);
- [Breve guia sobre Análise estática de código](https://deepsource.io/blog/static-analysis-javascript/).
