% Test-Driven Development (TDD)
% Alex Kutzke
% Agosto 2023

# Testes Unitários (Unit Tests)

## Vários tipos de testes

- Unitário (*unit*); 
- Aceitação (*acceptance*);
- Exploratório (*exploratory*);
- Usabilidade (*usability*); 
- ...

[Glossário Agile Alliance](https://www.agilealliance.org/agile101/agile-glossary/)

. . .

Focaremos em testes unitários.

## Teste Unitário

> A unit test, as Agile teams understand the term, is a short program fragment written and maintained by the developers on the product team, which exercises some narrow part of the product’s source code and checks the results. 
[[1]](https://www.agilealliance.org/glossary/unit-test/)

> Um teste unitário, como as equipes ágeis entendem o termo, é um pequeno fragmento de programa escrito e mantido pelos **desenvolvedores** na equipe do produto, que executa uma parte restrita do código-fonte do produto e verifica os resultados.

## Como?

Testes unitários podem ser realizados de diferentes maneiras. De simples verificações, com estruturas **nativas** de linguagens, até **frameworks especializados**.

Um exemplo bastante simples em JS:

```{.js}
class Calculator {
  static sum(a,b) {
    return(a+b+1);
  }
}

console.assert(Calculator.sum(2,2) === 4);
```

## Exemplo JS com Jest

```{.js .r-stretch}
import { Allergies } from './allergies';

describe('Allergies', () => {
  describe('testing for eggs allergy', () => {
    test('not allergic to anything', () => {
      const allergies = new Allergies(0);
      expect(allergies.allergicTo('eggs')).toEqual(false);
    });

    test('allergic only to eggs', () => {
      const allergies = new Allergies(1);
      expect(allergies.allergicTo('eggs')).toEqual(true);
    });

    test('allergic to eggs and something else', () => {
      const allergies = new Allergies(3);
      expect(allergies.allergicTo('eggs')).toEqual(true);
    });
    // ...
  }
}
```

## Unit Test Frameworks

- Cada linguagem de programação possui seus próprios frameworks para testes unitários (e outros tipos):

[List of unit testing frameworks](https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks#JavaScript)

- Hoje, estudaremos o **Jest** para **Javascript**.

# Jest

## Jest

Segundo o próprio site [jestjs.io](https://jestjs.io):

> Jest é um poderoso Framework de Testes em JavaScript com um foco na simplicidade.

Funciona com: Babel, TypeScript, Node, React, Angular, Vue e outros ...

## Instalação

Para testarmos o Jest, primeiro vamos criar um projeto simples JS:

```shell
$ mkdir calculator
$ cd calculator
$ npm init 
```

E instalamos o **Jest** utilizando o próprio `npm`:

```shell
$ npm install jest
```

Adicione o seguinte no arquivo `package.json`:

```json
{
  "scripts": {
    "test": "jest"
  }
}
```

## Primeiro arquivo

Vamos criar uma calculadora simples (`calculator.js`):

```js
class Calculator {
  static sum(a,b) {
    return(a+b);
  }
}

module.exports = Calculator;
```

## Nosso primeiro teste

Crie um novo arquivo, chamado `calculator.test.js`:

```js
const Calculator = require('./calculator');

test('2 + 2 equals 4', () => {
  expect(Calculator.sum(2,2)).toEqual(4);
});
```

Para testar, execute `npm test`.

## Saída

```sh
> calculator@1.0.0 test
> jest

PASS ./calculator.test.js
  ✓ 2 + 2 equals 4 (3 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        0.234 s, estimated 1 s
Ran all test suites.
```

## Babel

O Jest é compatível com [Babel](https://babeljs.io/). Só é necessário um pouco de [configuração](https://babeljs.io/setup#installation):

Adicione o seguinte ao arquivo `package.json`:

```json
  "jest": {
    "transform": {
      "^.+\\.[t|j]sx?$": "babel-jest"
    }
  }
```

Instale um *preset* do Babel:

```bash
$ npm install @babel/preset-env --save-dev
```

Crie o arquivo `babel.config.json` com o seguinte conteúdo:

```json
{
  "presets": ["@babel/preset-env"]
}
```

## Babel

Agora podemos utilizar sintaxes mais modernas do JS (como imports) sem dificuldades:

```js
class Calculator {
  static sum(a,b) {
    return(a+b);
  }
}

export default Calculator;
```

```js
import Calculator from './calculator';

test('2 + 2 equals 4', () => {
  expect(Calculator.sum(2,2)).toEqual(4);
});
```

## Anatomia do teste

```js
test('2 + 2 equals 4', () => {
  expect(Calculator.sum(2,2)).toEqual(4);
});
```

- Nome;
- Função a ser executada com um **resultado esperado**;

## Objeto `expect`

> Quando você está escrevendo testes, muitas das vezes você precisa checar se os valores satisfazem certas condições. expect lhe dá acesso a inúmeros "matchers" que permitem validar diferentes coisas.

```{.js data-line-numbers="2"}
test('2 + 2 equals 4', () => {
  expect(Calculator.sum(2,2)).toEqual(4);
});
```

## Matchers

Métodos de `expect` que permitem a checagem de **diferentes tipos de valores**:

- `.toBe()`: checagem de igualdade;
- `.toEqual()`: checagem de valor;
- `.toBeNull()`: checagem de `null`;
- `.toBeCloseTo()`: checagem aproximada numérica;
- `.toContain()`: checagem de pertinência;
- `.toThrow()`: checagem de exceção;
- e [vários outros](https://jestjs.io/docs/using-matchers).

## Prática

Implemente a função abaixo e crie testes para ela (por enquanto, não se preocupe com método):

```js
var a = [1,2,3];

sum(a);
// -> 6
```

## Outros recursos

O Jest, assim como outros frameworks de testes, possui outros recursos interessantes. Por exemplo:

- [Setups e Teardowns](https://jestjs.io/docs/setup-teardown);
- [Scoping](https://jestjs.io/docs/setup-teardown#scoping);
- [Testes para códigos assíncronos](https://jestjs.io/docs/asynchronous);
- [Mocks](https://jestjs.io/docs/mock-functions).

Esses recursos são importantes para mantermos boas práticas de desenvolvimento dos testes.

# Test-Driven Development

## O que é?

> "A style of programming in which three activities are tightly interwoven: coding, testing (in the form of writing unit tests) and design (in the form of refactoring)." [[1]]()https://www.agilealliance.org/glossary/tdd/)

> "Um estilo de programação no qual três atividades são fortemente entrelaçadas: **codificação**, **teste** (na forma de testes unitários) e **design** (na forma de refatoração)."

## Como assim?

Simples: escrever testes unitários **antes** de código de produção.

## As Três leis do TDD

1. Você não deve escrever código de produção até que tenha escrito um teste unitário falho;
2. Você não deve escrever um código de teste além do necessário para que este seja falho (não compilar também é uma falha);
3. Você não deve escrever código de produção além do necessário para que o teste atual (até então falho) passe;

## Em outras palavras

1. Escrever um teste unitário "único" descrevendo uma funcionalidade do programa;
2. Executar o teste, que deve falhar porque o programa não possui essa funcionalidade implementada;
3. Escrever código "apenas suficiente", o mais simples possível, para fazer a passagem do teste;
4. Refatorar o código até que esteja em conformidade com o critério mais simples;
5. Acumular testes unitários ao longo do tempo.

## Ciclos curtos

Assim, temos ciclos bastante curtos entre escrita de teste e de código de produção.

Curtos mesmo, entre segundos e poucos minutos.

## Tem certeza?

Mas assim não teremos basicamente a mesma quantidade de código de teste e de produção?

. . .

Sim!

. . .

Mas teremos também uma alta taxa de **cobertura** (*coverage*) do código, permitindo a aplicação da principal estratégia de desenvolvimento ágil: **refatoração sem medo**.

## Benefícios esperados

Por trás de um suposto "sobre trabalho", o TDD entrega alguns benefícios:

- Refatoração sem medo;
- Redução na taxa de bugs;
- Nos força a pensar a partir de um outro ponto de vista: do utilizador do programa. Assim, tornamos o programa "convenientemente chamável (*callable*)";
- Nos força a escrever programas testáveis;
- Programas **testáveis e chamáveis** devem ser isolados:
  - Ou seja, geramos códigos desacoplados, independentes, desde o início (tomamos decisões de **design** enquanto escrevemos testes);
- Produzimos uma documentação verdadeira.

## Custo

Mas é claro, tem-se custos:

- Curva de aprendizagem;
- Tempo inicial de desenvolvimento é um pouco maior;
- Clean code também em testes:
  - Teste mal escrito é um teste que será esquecido, e se tornará mais improdutivo do que um comentário mal feito.

## Mas antes, uma primeira experiência de TDD

Vamos tentar criar a classe Stack que implementa uma pilha simples, com seus métodos clássicos: `push`, `pop` e `empty`.

Seguiremos as regras do TDD.

![Pilha de pratos](./img/pratos.jpg)

## Pilha

![Operações push e pop](./img/pilha.png)

## Clean Tests

- Legibilidade, legibilidade e legibilidade.
- Todas as ideias de Clean Code continuam válidas para testes;
- E mais algumas.

## Linguagens de Domínio Específico (DSL)

Para além do uso de Frameworks de testes, crie funções que auxiliem na leitura de seus testes.

Essas funções podem ser utilizadas em diversos testes, tornando-se um padrão em seus testes.

## Exemplo

Muitos detalhes tiram atenção do que está sendo de fato, testado.

```{.java .r-stretch}
public void testGetPageHieratchyAsXml() throws Exception
{
  crawler.addPage(root, PathParser.parse("PageOne"));
  crawler.addPage(root, PathParser.parse("PageOne.ChildOne"));
  crawler.addPage(root, PathParser.parse("PageTwo"));
  
  request.setResource("root");
  request.addInput("type", "pages");
  Responder responder = new SerializedPageResponder();
  SimpleResponse response =
    (SimpleResponse) responder.makeResponse(
        new FitNesseContext(root), request);
  String xml = response.getContent();

  assertEquals("text/xml", response.getContentType());
  assertSubString("<name>PageOne</name>", xml);
  assertSubString("<name>PageTwo</name>", xml);
  assertSubString("<name>ChildOne</name>", xml);
}
```

## Exemplo refatorado

```{.java .numberLines data-line-numbers="1-10|6-9"}
public void testGetPageHierarchyAsXml() throws Exception {
  makePages("PageOne", "PageOne.ChildOne", "PageTwo");
  
  submitRequest("root", "type:pages");

  assertResponseIsXML();
  assertResponseContains(
    "<name>PageOne</name>", "<name>PageTwo</name>", "<name>ChildOne</name>"
  );
}
```

## Padrão duplo

Embora código limpo seja importante tanto em testes quando em produção, é importante lembrar que testes rodam em **ambiente de testes**.

Isso permite que algumas concessões sejam feitas. Principalmente no que diz respeito à desempenho. (Não quer dizer que o teste pode ser lento).

## Exemplo

```{.java .numberLines }
@Test
public void turnOnLoTempAlarmAtThreashold() throws Exception {
  hw.setTemp(WAY_TOO_COLD);
  controller.tic();
  assertTrue(hw.heaterState());
  assertTrue(hw.blowerState());
  assertFalse(hw.coolerState());
  assertFalse(hw.hiTempAlarm());
  assertTrue(hw.loTempAlarm());
}
```

## Exemplo Refatorado

```{.java .numberLines }
@Test
public void turnOnLoTempAlarmAtThreshold() throws Exception {
  wayTooCold();
  assertEquals("HBchL", hw.getState());
}
```

```{.java .numberLines }
public String getState() {
  String state = "";
  state += heater ? "H" : "h";
  state += blower ? "B" : "b";
  state += cooler ? "C" : "c";
  state += hiTempAlarm ? "H" : "h";
  state += loTempAlarm ? "L" : "l";
  return state;
}
```

## Exemplo como DSL

```{.java .numberLines .r-stretch} 
@Test
public void turnOnCoolerAndBlowerIfTooHot() throws Exception {
  tooHot();
  assertEquals("hBChl", hw.getState());
}
@Test
public void turnOnHeaterAndBlowerIfTooCold() throws Exception {
  tooCold();
  assertEquals("HBchl", hw.getState());
}
@Test
public void turnOnHiTempAlarmAtThreshold() throws Exception {
  wayTooHot();
  assertEquals("hBCHl", hw.getState());
}
@Test
public void turnOnLoTempAlarmAtThreshold() throws Exception {
  wayTooCold();
  assertEquals("HBchL", hw.getState());
}
```

## Um teste por conceito

- Cada função de teste deve ter apenas um conceito (ideia) como alvo;

```{.java .numberLines .r-stretch}
/**
* Miscellaneous tests for the addMonths() method.
*/
public void testAddMonths() {
  SerialDate d1 = SerialDate.createInstance(31, 5, 2004);

  SerialDate d2 = SerialDate.addMonths(1, d1);
  assertEquals(30, d2.getDayOfMonth());
  assertEquals(6, d2.getMonth());
  assertEquals(2004, d2.getYYYY());

  SerialDate d3 = SerialDate.addMonths(2, d1);
  assertEquals(31, d3.getDayOfMonth());
  assertEquals(7, d3.getMonth());
  assertEquals(2004, d3.getYYYY());

  SerialDate d4 = SerialDate.addMonths(1, SerialDate.addMonths(1, d1));
  assertEquals(30, d4.getDayOfMonth());
  assertEquals(7, d4.getMonth());
  assertEquals(2004, d4.getYYYY());
}
```

## F.I.R.S.T.

- **Fast**: Testes não devem demorar;
- **Independent**: Testes não devem depender entre si (aqui, Mocks ajudam);
- **Repeatable**: Testes devem rodar em qualquer ambiente (na empresa, em casa, com rede, sem rede, etc);
- **Self Validating**: Testes devem retornar um resultado booleano. Pass ou Fail;
- **Timely**: Testes devem ser escritos imediatamente **antes** do código de produção.


## Referências

- Martin, Robert C. Clean code: a handbook of agile software craftsmanship. Pearson Education, 2009.
- Martin, Robert C. Agile software development: principles, patterns, and practices. Pearson New International Edition, First Edition, 2014.
- Agile Alliance Glossary <https://www.agilealliance.org/agile101/agile-glossary/>. Acessado em: 20/08/21.
- Jest <https://jestjs.io>. Acessado em: 20/08/21.
- Babel <https://babeljs.io>. Acessado em: 20/08/21.
