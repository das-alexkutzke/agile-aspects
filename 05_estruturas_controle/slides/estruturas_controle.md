% Clean Code: estruturas de controle e outras práticas
% Alex Kutzke
% Agosto 2023

# Estruturas de Controle

## Estruturas de Controle

- Estruturas de controle como `if`, `while`, `for` estão presentes em qualquer código;
- Mas, **se mal utilizadas**, podem levar a códigos ruins;
- Observaremos 3 areas principais sobre estruturas de controle e clean code:

  1. Preferir checagens positivas;
  2. Evitar aninhamentos profundos;
  3. Adotar tratamento de erros.

# Prefira Checagens positivas

## Checagens positivas

- Condicionais escritos com palavras afirmativas auxiliam a interpretação:


```{.js .numberLines}
if (isEmpty(blogContent)) {
  // throw error
}
 
if (!hasContent(blogContent)) {
  // throw error
}
```

# Evite aninhamentos profundos

## Aninhamentos profundos

- Estruturas com vários níveis de aninhamento dificultam a leitura e a manutenção do código; 
- Limpar aninhamentos profundos nem sempre é uma tarefa fácil;
- Mas podemos fazer uso de algumas técnicas como:
  
  1. Uso de "guardas" ou *"fail fast"*;
  2. Extração de estruturas de controle para funções;
  3. Polimorfismo e funções fábrica;
  4. Substituição de condicionais por tratamento de erros.

## Uso de "guardas"

- Mover condicionais que **checam falhas** para o topo de funções;
- Dessa forma, fazemos checagens que corroboram uma abordagem *"fail fast"*;

```{.js .numberLines}
function doStuff(){
  if (cond1) {
    if(cond2) {
      //...
      console.log("Do that stuff");
    }
  }
}
```

. . . 

```{.js .numberLines}
function doStuff(){
  // Guard to fail fast (i know, this is a bad comment :) )
  if(!cond1 || !cond2 || ...){
    return;
  }
  console.log("Do that stuff");
}
```


## Exemplo: uso de "guardas"

Adicione condicionais "guardas" no código abaixo:

```{.js .numberLines}
function messageUser(user, message) {
  if (user) {
    if (message) {
      if (user.acceptsMessages) {
        const success = user.sendMessage(message);
        if (success) {
          console.log('Message sent!');
        }
      }
    }
  }
}
```

## Solução

```{.js .numberLines}
function messageUser(user, message) {
  if (!user || !message || !user.acceptsMessages) {
    return;
  }

  const success = user.sendMessage(message);
  if (success) {
    console.log('Message sent!');
  }
}
```

## Extração de Funções

Estruturas de controle aninhadas podem ser extraídas para funções com um bom nome:

```{.js .r-stretch .numberLines data-line-numbers="1-16|2-4|9-13" }
function connectDatabase(uri) {
  if (!uri) {
    throw new Error('An URI is required!');
  }
  
  const db = new Database(uri);
  let success = db.connect();
  if (!success) {
    if (db.fallbackConnection) {
      return db.fallbackConnectionDetails;
    } else {
      throw new Error('Could not connect!');  
    }
  }
  return db.connectionDetails;
}
```

## Solução: Extração de Funções

```{.js .numberLines }
function connectDatabase(uri) {
  validateUri(uri);

  const db = new Database(uri);
  let success = db.connect();
  let connectionDetails;
  if (success) {
    connectionDetails = db.connectionDetails;
  } else {
    connectionDetails = connectFallbackDatabase(db);
  }
  return connectionDetails;
}

function validateUri(uri) {
  if (!uri) {
    throw new Error('An URI is required!');
  }
}

function connectFallbackDatabase(db) {
  if (db.fallbackConnection) {
    return db.fallbackConnectionDetails;
  } else {
    throw new Error('Could not connect!');  
  }
}
```

## Polimorfismo e Funções Fábrica

- Para aqueles casos onde temos vários condicionais parecidos;
- Sim, aqueles casos em que um `switch` parece uma boa ideia;

> Switch único: não pode existir mais de um switch para um dado tipo de seleção. Os *cases* desse switch devem criar objetos polimórficos que serão colocados no lugar de outros switch's do código.

Ou ...

> Esconda o switch em uma função ou classe de baixo nível e nunca permita que ele se repita.

## Exemplo: Polimorfismo e Funções Fábrica

```{.js .r-stretch .numberLines }
function renderShape(shape){
  switch(shape.type){
    case "square":
      renderSquare(shape);
      break;
    case "rectangle":
      renderRectangle(shape);
      break;
    case "circle":
      renderCircle(shape);
      break;
    default:
      throw new Error('Unknown shape.');
  }
}
```

## Exemplo: Polimorfismo e Funções Fábrica

```{.js .r-stretch .numberLines data-line-numbers="1-18|2|7-18" }
function renderShape(shape){
  const shapeRender = getShapeRender(shape);

  shapeRender(shape);
}

function getShapeRender(shape){
  switch(shape.type){
    case "square":
      return renderSquare;
    case "rectangle":
      return renderRectangle;
    case "circle":
      return renderCircle;
    default:
      throw new Error('Unknown shape.');
  }
}
```

## Polimorfismo e Funções Fábrica

- `getShapeRender` é uma fábrica que sempre produz uma função que é utilizada da mesma forma, qualquer que seja o caso (polimórfico);
- O mesmo comportamento pode ser alcançado por meio de Classes e Heranças.

```{.js .r-stretch .numberLines }
function getShapeRender(shape){
  switch(shape.type){
    case "square":
      return renderSquare;
    case "rectangle":
      return renderRectangle;
    case "circle":
      return renderCircle;
    default:
      throw new Error('Unknown shape.');
  }
}
```

## Polimorfismo e Funções Fábrica (classes)

```{.java .r-stretch .numberLines }
public Money calculatePay(Employee e)
  throws InvalidEmployeeType {
    switch (e.type) {
      case COMMISSIONED:
        return calculateCommissionedPay(e);
      case HOURLY:
        return calculateHourlyPay(e);
      case SALARIED:
        return calculateSalariedPay(e);
      default:
        throw new InvalidEmployeeType(e.type);
    }
  }
```

## Polimorfismo e Funções Fábrica (classes)

```{.java .numberLines }
public abstract class Employee {
  public abstract boolean isPayday();
  public abstract Money calculatePay();
  public abstract void deliverPay(Money pay);
}
-----------------
public interface EmployeeFactory {
  public Employee makeEmployee(EmployeeRecord r) throws InvalidEmployeeType;
}
-----------------
public class EmployeeFactoryImpl implements EmployeeFactory {
  public Employee makeEmployee(EmployeeRecord r) throws InvalidEmployeeType {
    switch (r.type) {
      case COMMISSIONED:
        return new CommissionedEmployee(r) ;
      case HOURLY:
        return new HourlyEmployee(r);
      case SALARIED:
        return new SalariedEmploye(r);
      default:
        throw new InvalidEmployeeType(r.type);
    }
  }
}
```

# Adote tratamento de erros

## Adote tratamento de erros

- Por vezes, criamos lógicas de retornos e if desnecessários para lidar com erros;
- As funcionalidades de tratamentos de erros das linguagens modernas podem nos ajudar a eliminar essas estruturas confusas;


## Exemplo: Adote tratamento de erros

```{.js .r-stretch .numberLines data-line-numbers="1-19|4-7|13,17" }
function createUser(email, password) {
  const inputValidity = validateInput(email, password);
  
  if (inputValidity.code === 1 || inputValidity === 2) {
    console.log(inputValidity.message);
    return;
  }
  // ... continue
}
 
function validateInput(email, password) {
  if (!email.includes('@') || password.length < 7) {
    return { code: 1, message: 'Invalid input' };
  }
  const existingUser = findUserByEmail(email);
  if (existingUser) {
    return { code: 2, message: 'Email is already in use!' };
  }
}
```

## Exemplo: Adote tratamento de erros

```{.js .r-stretch .numberLines }
function createUser(email, password) {
  try {
    validateInput(email, password);  
  } catch (error) {
    console.log(error.message);
  }
  // ... continue
}
 
function validateInput(email, password) {
  if (!email.includes('@') || password.length < 7) {
    throw new Error('Input is invalid!');
  }
  const existingUser = findUserByEmail(email);
  if (existingUser) {
    throw new Error('Email is already taken!');
  }
}
```

## Erros são uma apenas coisa

- Pode ser interessante mover o tratamento de erros para sua própria função ou método:

```{.js .r-stretch .numberLines }
function handleSignupRequest(request) {
  try {
    createUser(request.email, request.password)
  } catch (error) {
    showErrorMessage(error.message);
  }
}
 
function createUser(email, password) {
  validateInput(email, password);  
  // ... continue
}
```

## Referências

- Martin, Robert C. Clean code: a handbook of agile software craftsmanship. Pearson Education, 2009.
- Schwarzmüller, Maximilian. Clean code course. Academind, 2021.
