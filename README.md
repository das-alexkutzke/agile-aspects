Especialização em Desenvolvimento Ágil de Software

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Aspectos Ágeis de Programação*

Prof. Alexander Robert Kutzke

# Material do módulo Aspectos Ágeis de Programação

## Aulas

* [Aula 01 - Introdução ao Clean Code](01_introducao/slides/introducao.md);
* [Aula 02 - Nomes](02_nomes/slides/nomes.md);
* [Aula 03 - Funções e Métodos](03_funcoes_e_metodos/slides/funcoes_e_metodos.md);
* [Aula 04 - Comentários e Estrutura de Código](./04_comentarios_formatacao/slides/comentarios_formatacao.md);
* [Aula 05 - Estruturas de Controle](./05_estruturas_controle/slides/estruturas_controle.md);
* [Aula 06 - Code Smells](./06_code_smells/code_smells.md);
* [Aula 07 - TDD](./07_tdd/slides/tdd.md);
* [Aula 08 - TDD e Angular](./08_angular_testing/slides/angular_testing.md).
* [Aula 09 - Pair programming](./09_pair_programming/slides/pair_programming.md).

- [Links interessantes](./links.md).
