% Pair Programming
% Alex Kutzke
% Setembro 2023

# Introdução

## O que é?

> Write all production programs with two people sitting at one machine.
> 
> -- Kent Beck: "Extreme Programming"

## Definição

- De maneira geral, é processo em que duas pessoas trabalham no código na mesma máquina, ao mesmo tempo;
- Forma colaborativa de trabalho:
  - Envolve muita comunicação:
  - Não apenas programar, mas planejar e discutir o próprio trabalho;
  - Esclarecer ideias, discutir abordagens e chegar a melhores soluções;
  - Compartilhar conhecimento entre a equipe (collective code ownership);

## Algumas considerações

- Deve ser opcional, programadores decidem;
- Sem cronograma prévio: depende da necessidade;
- É de curta duração:
  - Entre 15 minutos e um dia de trabalho;
  - Geralmente, entre uma ou duas horas;

## Algumas considerações

> "Durante uma semana, cada programador gastará cerca de metade
> do tempo da programação em dupla em suas próprias tarefas,
> solicitando a ajuda de muitos outros. A outra metade do tempo
> da programação será gasta auxiliando outros profissionais em suas tarefas."
>
> -- Robert Martin, p. 193.

## Motivação

> "Agora, se você é um gerente que se vê tentado a interferir na programação
> em dupla porque tem medo de que ela seja ineficiente, deixe seus medos de lado
> e deixe que os programadores descubram por eles. Afinal de contas, eles
> são os especialistas. ..."

## Motivação

> "E se você é um programador cujo gerente disse para interromper a programação
> e dupla, lembre-o de que você é o especialista e que, portanto, você, e não o gerente,
> deve ser o responsável pela forma como trabalha."
>
> "Finalmente, nunca, jamais, peça permissão para a programação em dupla. Ou para testar.
> Ou para refatorar. Ou ... Você é o especialista. Você decide."
>
> -- Robert Martin, p. 198.

# Como aplicar o Pair Programming

## Estilos: Piloto e Navegador

- **Piloto**: fica na direção... no teclado:
  - Foco nos pequenos objetivos;
  - Ignora questões maiores no momento;
  - Sempre deve verbalizar o que está fazendo;
- **Navegador**: posição de observação:
  - Revisa o código enquanto é escrito;
  - Dá direções e sugere ideias;
  - Pensa sobre questões mais amplas, bugs;
  - Toma nota de prováveis próximos passos e/ou obstáculos.

## Estilos: Piloto e Navegador

- Objetiva ter duas perspectivas para o mesmo código;
- Piloto pensa de forma mais tática, sobre detalhes;
- Navegador pensa de forma mais estratégica.

## Piloto e Navegador: fluxo 

- Inicia-se com uma tarefa bem definida;
- Concordância com um pequeno objetivo por vez (um teste unitário, por exemplo);
- Troca de papéis de formar regular:
  - Ajuda a manter o engajamento;
  - Favorece a aprendizagem;
- Navegador:
  - Entre no papel;
  - Evite pensamento tático... deixe detalhes para o piloto;
  - Tome um passo atrás e analise de forma mais ampla;
  - Tente não interromper o fluxo de trabalho do piloto.
  
## Estilos: Ping Pong

- Focado em TDD;
- "Ping": Desenvolvedor A escreve um teste que falha;
- "Pong": Desenvolvedor B escreve a implementação para passar o teste;
- Opcionalmente, a cada teste, fazer uma rodada de refatoração juntos:
  - Estratégia "Red - Green - Refactor".

## Estilos: Strong-Style Pairing

 > "Para uma ideia sair da sua cabeça e ser passada ao computador, ela precisa passar pelas mãos de outra pessoa."

- Geralmente utilizado para transferência de conhecimento;
- Piloto: alguém com menos experiência na atividade a ser executada (provavelmente novato);
- Navegador: alguém com experiência e conhecimento da atividade;

## Estilos: Strong-Style Pairing

- "Aprender fazendo";
- Piloto deve confiar no navegador e deve estar confortável em não compreender cada detalhe;
- Ao final, uma conversa para responder dúvidas deve ser realizada;
- É interessante para "on boarding", mas não deve ser sobre utilizada.

## Práticas interessantes

- Antes de iniciar a codificação, analise o problema em conjunto:
  - Entenda o problema;
  - Ache uma possível solução;
  - Planeje a abordagem;
- Se pesquisas forem necessárias:
  - Determine o que é necessário saber;
  - Realize buscas de forma separada;
  - Em seguida, compare os resultados obtidos;
- Aproveite o trabalho em dupla e já pense sobre a documentação:
  - Trabalho em dupla ajuda a manter nossa honestidade com certas tarefas. :)

## Controle do Tempo

- Faça um controle do tempo de trabalho:
  - Seja para descansos ou para trocas de papéis;
  - Previsibilidade é importante;
- Defina um tempo máximo para o trabalho em dupla;
- Técnicas como "Pomodoro" costumam funcionar bem com pairing;
- Nos momentos de pausa, realmente faça uma pausa.

## Rotação de pares

- É importante que as duplas mudem com o tempo;
- Evita que ambos fiquem "casados" do problemas e da própria dupla;
- Aumenta a propriedade coletiva do código;
- Favorece o desenvolvimento das habilidades de "troca de contexto" e de "on boarding";

## Planejamento do dia

- É necessário que a programação em duplas seja colocada na agenda de ambos;
- Deve ser um compromisso;
- Não deve ser interrompido, por exemplo, por uma reunião;
- A equipe, como um todo, pode definir um horário no dia, ou na semana, para a prática do pair programming.

## Setup

- Trabalhar juntos em uma mesma mesa exige um certo nível de respeito e atenção às necessidades do par;
- Libere espaço na mesa;
- Decidam sobre o uso de mais de um teclado ou monitor, por exemplo;
- Verifique se seu par tem alguma preferência ou necessidade particular (tamanhos de fonte, contraste, espaço físico, etc.);
- Se você usa algo fora do normal (layout de teclado, IDE, etc.), talvez seja importante trocar momentaneamente para algo em que ambos estejam à vontade.

## Remote Pairing

- Use vídeo;
- Utilize ferramentas para planejamento e design colaborativos (rascunhos, ferramentas visuais);
- Prefira espaços silenciosos e bons equipamentos de audio.

## Celebrem juntos

- Ao concluir uma tarefa em dupla, tome um tempo para conversar e celebrar;
  - Seja remota ou presencialmente.

## O que evitar

- Evite distrações:
  - Não confira emails ou seu celular;
  - Se for inevitável, seja claro e explique o que precisa fazer;
- Controle o modo de "Micro gerenciamento":
  - Dê tempo e espaço para que seu par pense e escreva o código;
  - Sem micro interrupções.

## O que evitar

- Impaciência:
  - Regra dos 5 segundos para navegadores: ao notar um erro, espere 5 segundos antes de apontá-lo;
- Não sequestre o teclado ou o computador, compartilhe; :)
- Sessões de programação em dupla não devem ser muito longas;

> Não existe um único jeito correto de se programar em duplas. Encontre o que funciona para você e seu par.

# Benefícios

## Benefícios

![Benefícios da programação em duplas](./img/benefits_overview.jpg)

## Compartilhamento de conhecimento

- Benefício direto;
- Não tenha medo de trabalhar em áreas nas quais você não tem conhecimento ou habilidade;
- Incentive que colegas façam pares diferentes para abordarem áreas diversas.

## Reflexão

- Força a verbalização de ideias;
- Favorece nossa reflexão;
- Precisa de uma atmosfera de confiança e solidariedade entre a dupla.

## Manter o foco

- Manutenção do foco por mais tempo;
- Comunicação quase que constante do que está pensado ou fazendo;

## Revisão de código

- Naturalmente, a revisão de código (ou uma espécie dela) ocorre durante a programação em dupla;
- Faça perguntas:
  - Se um dos integrantes não compreendeu um trecho de código, talvez seja interessante reescrevê-lo.

## Combinar dois modos de pensar

- Piloto e navegador acabam por analisar o código de maneiras diferentes;

## Propriedade coletiva do código

- Quanto maior a rotatividade das duplas, melhor;
- Mais pessoas conhecem mais partes do código;
- Potencialmente, elimina-se o problema de "só a pessoa X mexe naquela função".

## WIP pequeno

- Duas pessoas trabalhando em uma mesma tarefa, diminui o número de tarefas em andamento;
- Esse fator é importante para que o foco do time permaneça nas tarefas mais importantes.

## Rápida inclusão de novos membros

- Novos membros da equipe podem conhecer o código através de programação em dupla;
- Aprender fazendo;
- Quem já está na equipe pode revisitar e rever códigos;

# Desafios

## Pode ser cansativo

- Manter o foco é ótimo, mas também é cansativo;
- Não esqueça de fazer pausas (quantas forem necessárias);
- Não faça sessões muito longas de trabalho em dupla;

## Colaboração intensa é difícil

- Diferenças de conhecimento, técnica, personalidade e outras podem gerar atritos;
- Converse antes de começar e tente encontrar a melhor maneira de trabalhar juntos;

## Interrupções

- Evite da melhor maneira possível;
- Combine com a dupla e com a equipe.

## Diferença de experiência e habilidade

- Na realidade, pode ser produtivo;
- Pode gerar aprendizado e novos insights;
- Seja gentil.


## Dinâmicas de poder

- Por vezes, hierarquias e outras relações de poder podem gerar mal estar;
- Esteja atento a essas situações, principalmente se estiver no lado de maior poder;
- Seja gentil.

## Pairing precisa de vulnerabilidade

- Por vezes, pode ser difícil mostrar que você "não sabe algo", ou se sentir inseguro sobre uma decisão;
- Mostrar sua vulnerabilidade requer coragem e um ambiente seguro;
- Pessoas em posição de maior poder devem dar o exemplo.
- Seja gentil.

## Convencendo gerentes e colegas

- Difícil. :)
- Comece com poucos minutos;
- Sugira a introdução da prática como um teste;
- Fale sobre os benefícios;
- Faça por você.

# To pair or not to pair

## Tarefas chatas

- Evite realizar programação em duplas para tarefas chatas ou repetitivas;
- Reserve para algo desafiador ou para algo que você precisa aprender.

## "Eu conseguiria fazer isso sozinho?"

- O trabalho em dupla pode provocar insegurança, principalmente em quem está iniciado;
- É importante que novatos tenham um tempo de programação individual, para decantar o que foi aprendido.

## Code Review vs. Pairing

- Tarefas bastante distintas;
- Uma não impede a outra;

## Enfim, vale o esforço?

- Benefícios claros;
- Mas vão dizer que custa caro (depende);
- Precisa ser feito de uma forma inteligente.

## Referências

- Böckeler, B. e Siessegger, N. On Pair Programming <https://martinfowler.com/articles/on-pair-programming.html>. Acessado em: 28/08/23.
- Martin, Robert C. Desenvolvimento Ágil Limpo. Alta Books, 2020.
