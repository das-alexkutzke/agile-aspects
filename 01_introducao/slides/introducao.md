% Clean Code: Uma introdução
% Alex Kutzke
% Agosto 2023

# Por quê?

## Nossos objetivos

- Somos programadores, não?
- Queremos **ser programadores melhores**

- Queremos ser capazes de:
  - Diferenciar entre "código bom" e "código ruim"
  - Escrever "código bom"
  - Transformar "código ruim" em "código bom"

## Fim do código?

> "Código não é importante, devemos focar em modelos e requisitos"

> "Em breve, código será gerado ao invés de escrito"

## Código como especificação

- Código representa detalhes dos requisitos
- Especificar requisitos em tal detalhamento que uma máquina seja capaz de executá-los é **programação**
- Essa especificação é o **código**

## Existirá código

- Nível de abstração das linguagens de programação irá aumentar
- Podemos criar linguagens mais próximas dos requisitos
- Podemos criar ferramentas que nos ajudem a descrever os requisitos de maneira formal
- Mas, **nunca** eliminaremos a necessidade de precisão - assim, "existirá código"

# Código ruim

## Código ruim

- Premissa: "Código bom importa"

- Você já ficou significativamente impedido por um código ruim?

- Um verbo: **wade** -> "prosseguir com dificuldade"
  - "We wade through bad code"

- Quando analisamos um código ruim, torcemos por uma **dica** para entendê-lo

## O que fazemos sobre isso?

- Ficamos *wading* em códigos ruins, mas, mesmo assim, porque o escrevemos?
- Ou pior, por que continuamos **escrevendo**?

## O que fazemos sobre isso?

- Provavelmente estávamos:
  - Tentando escrever código com velocidade
  - E/ou sendo apressados
  - E/ou desmotivado
  - E/ou sobrecarregado
  - E/ou ...

## O que fazemos sobre isso? 

- Olhamos para nosso (ou de outros) código **bagunçado** e decidimos deixá-lo para *outro dia*

Todos passamos por isso :)

# O custo de uma bagunça

## O custo de uma bagunça

- Código bagunçado pode atrasar muito a evolução de projetos:
  - Alterações cada vez mais difíceis
  - Novas alterações geram novos problemas e mais bagunça
  - Produtividade cai

- Solução para um projeto lento? **Mais programadores** :)
  - Gera mais lentidão (ao menos a curto prazo)
  - Não conhecem o projeto
  - Serão treinados por quem fez a bagunça e olhando para a bagunça

## Produtividade X Tempo

![](./img/productivity_time_chart.png)

## E se fizermos um redesign?

- *Green field project*
- Precisa conversar com o projeto atual
- Pode demorar muito

## Atitude

- Nós, programadores, não somos profissionais

Chato né? =/

- E todo o resto (requisitos, gerentes, marketing, ...) não são culpados pelo código ruim?

Sim, mas muito mais os programadores

## Nossa parte da culpa

- Precisamos defender o bom código
- Inclusive, quando precisamos ir de encontro a decisões de outros profissionais
- Exemplo do Cirurgião ("scrubbing")
- Precisamos ser profissionais

## Dilema

- Programados enfrentam um "dilema":

Escrever código limpo X Respeitar deadlines?

- Falso paradoxo
- Código limpo (e o tempo que isso toma) irá nos levar a cumprir deadlines:
  - Inclusive deadlines de longo prazo

# Código limpo

## A arte do Código Limpo

- Conseguimos diferenciar um Código ruim de um Código bom
- Escrever Código Limpo é como pintar um quadro
- Em geral, conseguimos diferenciar uma bela pintura de uma mal feita:
  - Porém, uma minoria consegue realizar uma bela pintura
- Escrever Código Limpo exige disciplina
- Desenvolver um *Code-sense*
- Precisamos nos tornar artistas :)

## Mas o que é Código Limpo?

. . . 

- Robert Martin traz em seu livro várias opiniões sobre o que seria Código Limpo
- Gosto da resposta dada pelo Grady Booch:

> O código limpo é simples e direto.
> O código limpo é lido como uma prosa bem escrita. 
> O código limpo nunca obscurece a intenção do autor, mas sim 
> está cheia de abstrações nítidas e linhas de controle diretas.

## Mas o que é Código Limpo?

- Ou ainda o que respondeu Michael Feathers:

> Eu poderia listar todas as qualidades que noto no código limpo, mas
> existe uma qualidade abrangente que leva a todos eles.
> O código sempre parece ter sido escrito por alguém que se *importa*.
> Não há nada óbvio que você possa fazer para torná-lo melhor. Tudo
> já foi pensado pelo autor do código e se
> você tenta imaginar melhorias, você é levado de volta para onde você
> está, apreciando o código que alguém deixou para você -
> código deixado por alguém que se preocupa profundamente com o ofício.

## Sem resposta final

Definiremos o que é Código Limpo através do aprendizado de alguns aspectos importantes.

## Somos Autores

- Autores de códigos
- Autores tem leitores
- São responsáveis por se comunicar bem com os leitores
- Escreva uma linha de código pensando:

Um leitor vai ler essa linha e julgar meu esforço

Detalhe: nós somos grandes leitores dos nossos próprios códigos

## Esforço na escrita ou na leitura?

- Quanto um código é lido?
- Certamente mais lido do que escrito
- Boa parte de uma sessão de programação envolve ler o código escrito até o momento, bem como outros módulos e arquivos
- Robert Martin acredita que a razão entre leitura e escrita de código seja algo em torno de 10:1

## Escrever para uma leitura fácil

- Foco deve ser escrever código de fácil leitura
- Nem que isso implique em mais esforço na escrita
- Porém, escrever código legível, vai facilitar a próxima escrita

> "So if you want to go fast, if you want to get done quickly,
> if you want your code to be easy to write, make it easy to read."

## A regra do Escoteiro

"Deixe o acampamento mais limpo do que você encontrou"

- Escrever bem não é suficiente
- É necessário **manter o código limpo**
- Códigos "apodrecem" (*rot*)
- Mantenha sempre o código melhorando
- Não precisam ser mudanças grandes:
  - Um nome de variável, eliminar uma duplicação, limpar um comando `if` confuso ...

## A regra do Escoteiro

Esqueça aquela ideia de que não se deve mexer no que está funcionando. Isso vai contra qualquer ideia de desenvolvimento Ágil... durante o curso, tentaremos entender o por quê.

## Nosso caminho pelo Código Limpo

- Muito a se aprender
- Seguiremos os primeiros capítulos do livro *"Clean code: a handbook of agile software craftsmanship"* de Robert C. Martin:

1. Nomes
2. Estrutura de Código, Comentários e Formatação
3. Funções e Métodos
4. Estruturas de Controle e Erros
5. Objetos, Classes e outras Estruturas

## Referências

- Martin, Robert C. Clean code: a handbook of agile software craftsmanship. Pearson Education, 2009.
