% Clean Code: Comentários, formatação e estrutura de código
% Alex Kutzke
% Agosto 2023

# Comentários

## Comentários

> "Nada pode ser tão útil quanto um cometário bem colocado."

Robert C. Martin.

## Comentários

> "Nada pode confundir um módulo mais do que comentários dogmáticos e inúteis."

> "Nada pode ser tão prejudicial quanto um velho comentário mal-humorado que propaga mentiras e desinformação."

> "Comentários são, no melhor caso, um mal necessário."

Robert C. Martin.

## Comentários

- Comentários servem para compensar nossa falha em nos expressarmos por meio de código;
- O uso de comentários não deve ser algo a se celebrar;
- Mas, ainda sim, precisamos deles.

## Só mais uma frase

> Cada vez que você se expressa em código, você deve dar tapinhas nas costas. Cada vez que você escreve um comentário, você deve fazer uma careta e sentir o fracasso de sua capacidade de expressão.

Robert C. Martin.

## Por quê??

- Comentários mentem:
  - Nem sempre, nem intencionalmente;
  - Mas mentem;
- Códigos mudam e evoluem:
  - Comentários nem sempre acompanham essas mudanças;
  - Mudanças de local, de significado ou de importância;
  - É raro que comentários recebem a mesma atenção que o código aos serem alterados.

## Exemplo 

```{.java .numberLines}
MockRequest request;
private final String HTTP_DATE_REGEXP =
  "[SMTWF][a-z]{2}\\,\\s[0-9]{2}\\s[JFMASOND][a-z]{2}\\s"+
  "[0-9]{4}\\s[0-9]{2}\\:[0-9]{2}\\:[0-9]{2}\\sGMT";
private Response response;
private FitNesseContext context;
private FileResponder responder;
private Locale saveLocale;
// Example: ”Tue, 02 Apr 2003 22:18:49 GMT”
```

## Código limpo antes de comentários

- Programadores poderiam manter seus comentários **relevantes e precisos**;
- Mas essa energia pode ser empregada em **produzir código limpo** que nem precise de comentários.

## Comentários ruins

- Comentários incorretos ou pouco precisos são **muito piores** do que a ausência deles;
- Podem iludir e enganar. Enfim, mentir;
- Podem espalhar regras que já não valem mais e/ou levantar falsas expectativas;
- A verdade se encontra no código e apenas no código (nem na documentação):
  - Só o código pode contar o que ele faz;
- Portanto, minimizaremos os comentários, mas não os proibiremos.

## Comentários não melhoram código ruim

- Motivação para comentários: código mal escrito;
- "Hum que código confuso. Melhor comentá-lo."
  - Não! Melhor limpá-lo. ;)

## Explique-se através de código

O que é melhor?

```{.java .numberLines}
// Check to see if the employee is eligible for full benefits
if ((employee.flags & HOURLY_FLAG) && (employee.age > 65))
```

Ou ... 

```{.java .numberLines}
if (employee.isEligibleForFullBenefits())
```

## Bons comentários

- Veremos alguns comentários considerados bons;
- Mas, sempre lembre-se: os únicos comentários realmente bons são aqueles que você conseguiu **não** escrever.

## Comentários Legais

- Copyright;
- Licenças;
- Autoria;

```{.java .numberLines}
// Copyright (C) 2003,2004,2005 by Object Mentor, Inc. All rights reserved.
// Released under the terms of the GNU General Public License version 2 or later.
```

## Comentários Informativos

- Às vezes é interessante agregar informações básicas por meio de cometários:


```{.java .numberLines}
// Returns an instance of the Responder being tested.
protected abstract Responder responderInstance();
```

- Mas que tal trocar o nome do método para `responderBeingTested`?
- Outro exemplo:

```{.java .numberLines}
// format matched kk:mm:ss EEE, MMM dd, yyyy
Pattern timeMatcher = Pattern.compile(“\\d*:\\d*:\\d* \\w*, \\w* \\d*, \\d*”);
```

- Esse código poderia, ainda, ser colocado em uma classe responsável por essa conversão.

## Explicação de Intenção

```{.java .numberLines data-line-numbers="10"}
public int compareTo(Object o)
{
  if(o instanceof WikiPagePath)
  {
    WikiPagePath p = (WikiPagePath) o;
    String compressedName = StringUtil.join(names, "");
    String compressedArgumentName = StringUtil.join(p.names, "");
    return compressedName.compareTo(compressedArgumentName);
  }
  return 1; // we are greater because we are the right type.
}
```

## Explicação de Intenção

```{.java .numberLines}
public void testConcurrentAddWidgets() throws Exception {

  ...

  //This is our best attempt to get a race condition
  //by creating large number of threads.
  for (int i = 0; i < 25000; i++) {
  WidgetBuilderThread widgetBuilderThread = 
      new WidgetBuilderThread(widgetBuilder, text, parent, failFlag);
    Thread thread = new Thread(widgetBuilderThread);
    thread.start();
  }
  assertEquals(false, failFlag.get());
}
```

## Esclarecimento

- Por vezes você não pode alterar algum código, então um comentário de esclarecimento pode ser interessante:

```{.java .numberLines data-line-numbers="9-17"}
public void testCompareTo() throws Exception
{
  WikiPagePath a = PathParser.parse("PageA");
  WikiPagePath ab = PathParser.parse("PageA.PageB");
  WikiPagePath b = PathParser.parse("PageB");
  WikiPagePath aa = PathParser.parse("PageA.PageA");
  WikiPagePath bb = PathParser.parse("PageB.PageB");
  WikiPagePath ba = PathParser.parse("PageB.PageA");
  assertTrue(a.compareTo(a) == 0);    // a == a
  assertTrue(a.compareTo(b) != 0);    // a != b
  assertTrue(ab.compareTo(ab) == 0);  // ab == ab
  assertTrue(a.compareTo(b) == -1);   // a < b
  assertTrue(aa.compareTo(ab) == -1); // aa < ab
  assertTrue(ba.compareTo(bb) == -1); // ba < bb
  assertTrue(b.compareTo(a) == 1);    // b > a
  assertTrue(ab.compareTo(aa) == 1);  // ab > aa
  assertTrue(bb.compareTo(ba) == 1);  // bb > ba
}
```

- Todos os comentários estão corretos?

## Comentários de *Warning*

- Avisar sobre consequências:

```{.java .numberLines }
// have some time to kill.
public void _testWithReallyBigFile()
{
  writeLinesToFile(10000000);

  response.setBody(testFile);
  response.readyToSend(this);
  String responseString = output.toString();
  assertSubString("Content-Length: 1000000000", responseString);
  assertTrue(bytesSent > 1000000000);
}
```

- Poderíamos utilizar `@Ignore("Takes too long to run")` ou alguma outra forma de ignorar um teste da linguagem utilizada.

## Comentários *TODO*

```{.java .numberLines}
//TODO-MdM these are not needed
// We expect this to go away when we do the checkout model
protected VersionInfo makeVersion() throws Exception
{
  return null;
}
```

- Altamente debatível se é um bom tipo de comentário;
- Pode ser útil, mas não pode ser uma desculpa para deixar código sujo para trás;
- Plataformas de versionamento podem ser melhores para pontuar tarefas (issues e afins).

## Amplificação

- Amplificar a importância de algo que, do contrário, pareceria sem importância:


```{.java .numberLines }
String listItemContent = match.group(3).trim();
// the trim is real important.  It removes the starting
// spaces that could cause the item to be recognized
// as another list.
new ListItemWidget(this, listItemContent, this.level + 1);
return buildList(text.substring(match.end()));
```

## Comentários Ruins

- Desculpas para códigos ruins;
- Justificativas para decisões insuficientes;
- Programador falando com ele mesmo.

## "Resmungo"

- Difícil interpretação;

```{.java .numberLines }
public void loadProperties()
{
  try
  {
    String propertiesPath = propertiesLocation + "/" + PROPERTIES_FILE;
    FileInputStream propertiesStream = new FileInputStream(propertiesPath);
    loadedProperties.load(propertiesStream);
  }
  catch(IOException e)
  {
    // No properties files means all defaults are loaded
  }
}
```

- Não resta outra opção a não ser olhar em outras partes do código para entendermos:
  - O que caracteriza um péssimo comentário. =/

## Comentários Redundantes

- Nenhuma nova informação é adicionada;
- Ler o código seria suficiente e **mais preciso**.

```{.java .numberLines}
// Utility method that returns when this.closed is true. Throws an exception
// if the timeout is reached.
public synchronized void waitForClose(final long timeoutMillis)
  throws Exception
{
  if(!closed)
  {
    wait(timeoutMillis);
    if(!closed)
      throw new Exception("MockResponseSender could not be
          closed");
  }
}
```

## Comentários Redundantes

```{.java .r-stretch .numberLines}
public abstract class ContainerBase
implements Container, Lifecycle, Pipeline,
           MBeanRegistration, Serializable {

     /**
      * The processor delay for this component.
      */
     protected int backgroundProcessorDelay = -1;


     /**
      * The lifecycle event support for this component.
      */
     protected LifecycleSupport lifecycle =
       new LifecycleSupport(this);


     /**
      * The container event listeners for this Container.
      */
     protected ArrayList listeners = new ArrayList();
...
```

## Comentários que enganam

- Mesmo sem querer, podemos inserir informações que não são precisas;
- O código abaixo não retorna **quando** `this.closed` é `true`, mas sim, **se** `this.closed` é `true`, do contrário espera um *timeout*. Sutil, não?

```{.java .numberLines}
// Utility method that returns when this.closed is true. Throws an exception
// if the timeout is reached.
public synchronized void waitForClose(final long timeoutMillis)
  throws Exception
{
  if(!closed)
  {
    wait(timeoutMillis);
    if(!closed)
      throw new Exception("MockResponseSender could not be
          closed");
  }
}
```

## Comentários obrigatórios

- "Toda função precisa de comentário" ...
- "Toda variável precisa estar comentada" ...
* Abre espaço para comentários que mentem ou enganam.


```{.java .r-stretch .numberLines }
/**
 *
 * @param title The title of the CD
 * @param author The author of the CD
 * @param tracks The number of tracks on the CD
 * @param durationInMinutes The duration of the CD in minutes
 */
public void addCD(String title, String author,
    int tracks, int durationInMinutes) {
  CD cd = new CD();
  cd.title = title;
  cd.author = author;
  cd.tracks = tracks;
  cd.duration = duration;
  cdList.add(cd);
}
```

## Changelogs e "Diários"

```{.java .numberLines }
/*
* Changes (from 11-Oct-2001)
* --------------------------
* 11-Oct-2001 : Re-organised the class and moved it to new package
* 05-Nov-2001 : Added a getDescription() method, and eliminated NotableDate class (DG);
* 12-Nov-2001 : IBD requires setDescription() method, now that NotableDate
*               class is gone (DG);  Changed getPreviousDayOfWeek(),
*               getFollowingDayOfWeek() and getNearestDayOfWeek() to correct
*/
```

- Hoje, sistemas de versionamento fazem esse trabalho bem melhor:
  - Mas claro, com boas mensagens de *commit*. :)


## Ruídos

- Não agregam informação;

```{.java .numberLines }
/** The name. */
private String name;

/** The version. */
private String version;

/** The licenceName. */
private String licenceName;

/** The version. */
private String info;
```

- Além disso, está incorreto.

## Comentários que podem ser substituídos por funções ou variáveis


```{.java .numberLines }
// does the module from the global list <mod> depend on the
// subsystem we are part of?
if (smodule.getDependSubsystems().contains(subSysMod.getSubSystem()))
```

Poderia ser isso:

```{.java .numberLines }
ArrayList moduleDependees = smodule.getDependSubsystems();
String ourSubSystem = subSysMod.getSubSystem();
if (moduleDependees.contains(ourSubSystem))
```

## Código comentado

- Quem nunca?

```{.java .numberLines }
InputStreamResponse response = new InputStreamResponse();
response.setBody(formatter.getResultStream(),
    formatter.getByteCount());
// InputStream resultsStream = formatter.getResultStream();
// StreamReader reader = new StreamReader(resultsStream);
// response.setContent(reader.read(formatter.getByteCount()));
```

- Pode ser utilizado para debug, mas removido logo depois;
- Ao ver um código de outra pessoa, você tem coragem de apagar um código comentado?
  - Com um versionamento bem feito, sim.

# Formatação e estrutura de código

## Formatação

- Queremos que, ao olharem nosso código, percebam que profissionais trabalharam nele:
  - Que atenção de qualidade foi dada;
- Devemos deixar o código **bem formatado**;
- Um conjunto simples de regras deve ser definido e seguido por nós e pela equipe.

## A razão para formatação

- Formatação é importante;
- Trata de **comunicação**:
  - Comunicação é chave para desenvolvedores profissionais;

**Legibilidade antes de funcionalidade**

. . . 

Funcionalidades mudam, legibilidade permanece.

**Quais detalhes de formatação nos ajudam a comunicarmos melhor?**

## Formatação vertical

Quão longo um arquivo de código deve ser?

. . . 

É possível escrever bons projetos sem arquivos muito longos (menos de 500 linhas, talvez?).

Geralmente, **arquivos pequenos** são mais fáceis de serem entendidos.

## A metáfora da notícia

Nossos arquivos deveriam se parecer com artigos de notícias;

- Um título que diz do que se trata;
- Parágrafos iniciais com informações relevantes, porém ainda amplas;
- Parágrafos seguintes vão aprofundando detalhes;

## A metáfora da notícia

- Um nome simples mas explicativo (deve nos dizer se estamos no módulo correto ou não);
- Partes mais "no alto" devem apresentar conceitos de alto nível;
- Detalhes devem ir aumentando "abaixo" no código:
  - Até funções de mais baixo nível e outros detalhes.
- Um jornal é composto por várias notícias. Cada uma com sua própria história.

## Aberturas verticais entre conceitos

Cada linha de código representa uma **expressão** e cada grupo de linhas representa
um **pensamento** completo.

**Devemos separar** pensamentos com linhas em branco.

## Exemplo de aberturas verticais

```{.java .numberLines .r-stretch}
package fitnesse.wikitext.widgets;

import java.util.regex.*;

public class BoldWidget extends ParentWidget {
  public static final String REGEXP = "'''.+?'''";
  private static final Pattern pattern = Pattern.compile("'''(.+?)'''",
      Pattern.MULTILINE + Pattern.DOTALL
      );

  public BoldWidget(ParentWidget parent, String text) throws Exception {
    super(parent);
    Matcher match = pattern.matcher(text);
    match.find();
    addChildWidgets(match.group(1));
  }

  public String render() throws Exception {
    StringBuffer html = new StringBuffer(“<b>”);
    html.append(childHtml()).append(“</b>”);
    return html.toString();
  }
}
```

## Exemplo de aberturas verticais ruins

```{.java .numberLines .r-stretch}
package fitnesse.wikitext.widgets;
import java.util.regex.*;
public class BoldWidget extends ParentWidget {
  public static final String REGEXP = "'''.+?'''";
  private static final Pattern pattern = Pattern.compile("'''(.+?)'''",
      Pattern.MULTILINE + Pattern.DOTALL
      );
  public BoldWidget(ParentWidget parent, String text) throws Exception {
    super(parent);
    Matcher match = pattern.matcher(text);
    match.find();
    addChildWidgets(match.group(1));
  }
  public String render() throws Exception {
    StringBuffer html = new StringBuffer(“<b>”);
    html.append(childHtml()).append(“</b>”);
    return html.toString();
  }
}
```

## Densidade vertical

Se aberturas verticais separam conceitos, então códigos associados devem aparecer **próximos verticalmente**.

## Exemplo densidade vertical ruim

```{.java .numberLines }
public class ReporterConfig {

  /**
   * The class name of the reporter listener
   */
  private String m_className;

  /**
   * The properties of the reporter listener
   */
  private List<Property> m_properties = new ArrayList<Property>();

  public void addProperty(Property property) {
    m_properties.add(property);
  }
```

## Exemplo densidade vertical bom

```{.java .numberLines }
public class ReporterConfig {
  private String m_className;
  private List<Property> m_properties = new ArrayList<Property>();

  public void addProperty(Property property) {
    m_properties.add(property);
  }
```

## Distância vertical

- Conceitos relacionados devem estar em um mesmo arquivo (a não ser que exista uma boa razão para não estarem);
- A separação vertical de dois conceitos deve ser a medida de quão importantes eles são para que sejam entendidos;
- Queremos evitar que leitores tenham que "subir" e "descer" no código para entender algo.


## Distância vertical

- Declarar variáveis próximas de onde são utilizadas (depende da linguagem);
  - Variáveis de instância devem ficar em um lugar fixo (topo da classe, por exemplo);
- Funções dependentes deve estar próximas:
  - *Caller* acima da *callee*;
- Trechos de códigos com afinidade conceitual também devem estar próximos (funções ou variáveis com papéis similares, por exemplo).

## Formação Horizontal

Quão longa uma linha de código deve ser?

. . .

Linhas longas são difíceis de serem lidas.

Um limite seria não termos que rolar a tela.

Mas com monitores ultrawide, isso deixou de ser um critério.

Então, que tal, no máximo **120 caracteres**?

## Abertura horizontal e densidade

- A regra ainda vale horizontalmente: conceitos ligados devem aparecer próximos.
- Exemplo: atribuições e funções:

```{.java .numberLines data-line-numbers="3-4|5-6"}
private void measureLine(String line) {
  lineCount++;
  int lineSize = line.length();
  totalChars += lineSize;
  lineWidthHistogram.addLine(lineSize, lineCount);
  recordWidestLine(lineSize);
}
```

## Alinhamento Horizontal

Qual o problema desse tipo de alinhamento?

```{.java .numberLines .r-stretch}
public class FitNesseExpediter implements ResponseSender
{
  private   Socket          socket;
  private   InputStream     input;
  private   OutputStream    output;
  private   Request         request;
  private   Response        response;
  private   FitNesseContext context;
  protected long            requestParsingTimeLimit;
  private   long            requestProgress;
  private   long            requestParsingDeadline;
  private   boolean         hasError;
  public FitNesseExpediter(Socket          s, 
                           FitNesseContext context) throws Exception
  {
    this.context =            context;
    socket =                  s;
    input =                   s.getInputStream();
    output =                  s.getOutputStream();
    requestParsingTimeLimit = 10000;
  }
```

::: notes

Pode dar a impressão de que as colunas possuem significados relacionados. Ou ainda, esconder o fato que temos uma lista muito grande de coisas.

:::

## Indentação (escopos)

Qual é melhor?

```{.java .numberLines }
public class CommentWidget extends TextWidget
{
  public static final String REGEXP = "^#[^\r\n]*(?:(?:\r\n)|\n|\r)?";
  public CommentWidget(ParentWidget parent, String text){super(parent, text);}
  public String render() throws Exception {return ""; }
}
```

```{.java .numberLines }
public class CommentWidget extends TextWidget {
  public static final String REGEXP = "^#[^\r\n]*(?:(?:\r\n)|\n|\r)?";

  public CommentWidget(ParentWidget parent, String text) {
    super(parent, text);
  }

  public String render() throws Exception {
    return "";
  }
}
```

## *Dummy scopes*

Não deixe mais o `;` passar despercebido. :)

Troque

```{.java .numberLines }
while (dis.read(buf, 0, readBufferSize) != -1);
```

Por

```{.java .numberLines }
while (dis.read(buf, 0, readBufferSize) != -1)
  ;
```

## Regras e equipes

- A equipe deve acordar um estilo de formatação e segui-lo:
  - Tudo que for importante para a equipe:
    - Tamanho do recuo;
    - Onde `'{'` serão colocados;
    - Espaçamentos verticais e horizontais;
    - Etc...
- O código não pode parecer ter sido escrito por um bando de indivíduos descordantes;
- A última coisa que queremos é adicionar mais complexidade ao código.

## Referências

- Martin, Robert C. Clean code: a handbook of agile software craftsmanship. Pearson Education, 2009.
- Schwarzmüller, Maximilian. Clean code course. Academind, 2021.
