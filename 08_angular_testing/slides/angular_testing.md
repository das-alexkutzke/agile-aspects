% Testes Unitários com Angular
% Alex Kutzke
% Agosto 2021

# Angular e sua Plataforma de Testes

## Testes com angular

- O Angular vem preparado com uma robusta plataforma de testes;
- Os componentes principais são:
  - [*Jasmine Test Framework*](https://jasmine.github.io/);
  - [*Karma Test Runner*](https://karma-runner.github.io/);
  - Utilitários da API do Angular para auxiliar na criação dos testes (principalmente [*TestBed*](https://angular.io/api/core/testing/TestBed).
- Tudo já configurado para uso.

## Karma

A ferramenta *Karma* é, entre outras coisas, um executador de testes;

Para esse processo, a ferramenta adiciona várias funcionalidades, entre elas, uma pequena aplicação web para acompanhamento dos testes e seu debug.

![Karma](./img/karma.png)

## Jasmine

*Jasmine* é um framework de testes para JavaScript assim como o *Jest*; 

- São bastante semelhantes, em funcionalidade e em sintaxe;
- *Jasmine* é focado em Behavior-Driven Development (BDD):
  - Mas, para nós, isso não fará diferença agora.

![Jasmine](./img/jasmine.png)

## CLI para Testes

No Angular, para rodar os testes implementados, basta executar o seguinte comando:

```bash
$ ng test
```

Isso faz o *build* da aplicação em *watch mode* e já inicia o *Karma*.

## Aplicação Calculadora

Vamos criar uma nova aplicação para trabalhar com os testes:

```bash
$ ng new calculator
```

Aceite todos os valores *default*.

![Aplicação Calculadora](./img/calculator.png)

## Primeira execução de testes

Realize a primeira execução dos testes:

```bash
$ cd calculator
$ ng test
```

Alguns testes já estão implementados.

![Primeiro teste](./img/first_test.png)

## Cobertura de testes

Para verificar a cobertura de testes (*test coverage*), basta executar:

```sh
ng test --no-watch --code-coverage
```

Para uma visualização mais gráfica (*se é que existe alguém que não prefere a saída em terminal*), basta acessar o arquivo criado em `coverage/calculator/index.html`.

## O que testar?

No Angular, podemos criar *Unit Tests* para diferentes partes do framework. Por exemplo:

- *Services*;
- *Components*;
- *Directives*;
- *Pipes*;

Nesse aula, focaremos em testes para *Services* e *Components*.

## Antes, padrões ...

- Nomes: testes precisam ter o mesmo nome da unidade testada, com a extensão `.spec.ts`:
  - Exemplo: `app.component.ts` e `app.component.spec.ts`;
- Arquivos de testes devem ser armazenados **na mesma pasta** da unidade testada;

Muitos dos arquivos de teste que utilizaremos já serão criados automaticamente pelo Angular CLI.

# Testando Services

## Ideia

O teste de *Services* é, em geral, o mais simples. Uma primeira abordagem é apenas tratá-lo como uma classe normal:

```{.ts .r-stretch .numberLines data-line-numbers="1-25|3-4|6-8|10-16|18-24"}
// Straight Jasmine testing without Angular's testing support
describe('ValueService', () => {
  let service: ValueService;
  beforeEach(() => { service = new ValueService(); });

  it('#getValue should return real value', () => {
    expect(service.getValue()).toBe('real value');
  });

  it('#getObservableValue should return value from observable',
    (done: DoneFn) => {
    service.getObservableValue().subscribe(value => {
      expect(value).toBe('observable value');
      done();
    });
  });

  it('#getPromiseValue should return value from a promise',
    (done: DoneFn) => {
    service.getPromiseValue().then(value => {
      expect(value).toBe('promise value');
      done();
    });
  });
});
```

## Aplicação Calculadora - Serviço Soma

Para realizar testes de *Services* vamos avançar na nossa aplicação fictícia.

Vamos criar um novo serviço:

```bash
$ ng g service sum
CREATE coverage/calculator/sum.service.spec.ts (342 bytes)
CREATE coverage/calculator/sum.service.ts (132 bytes)
```

Este serviço irá, simplesmente, realizar a soma de dois números.

## `SumService`

```{.ts .r-stretch .numberLines data-line-numbers="1-13|10-12"}
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SumService {

  constructor() { }

  sum(num1: number, num2: number) : number {
    return(num1 + num2);
  }
}
```

Lembre-se de adicionar `SumService` como um `provider` em `app.module.ts`.

## Testes para SumService

O arquivo `sum.service.spec.ts` já possui um conteúdo pré-definido.

```{.ts .r-stretch .numberLines data-line-numbers="1-20"}
import { TestBed } from '@angular/core/testing';

import { SumService } from './sum.service';

describe('CalculatorService', () => {
  let service: SumService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SumService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
```

Execute `ng test` para abrir a interface de testes.

## TestBed

`TestBed` é um utilitário do Angular que auxilia a criação de testes que se adequem ao funcionamento do framework.

Na configuração inicial, ele já está presente.

```{.ts .r-stretch .numberLines data-line-numbers="1-20"}
import { TestBed } from '@angular/core/testing';

// ...

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SumService);
  });

// ...
```

## Criando alguns testes

Vamos criar um testes simples para o `SumService`:

```{.ts .r-stretch .numberLines }
describe('SumService', () => {

  // ...

  it('sum(2,3) should return 5', () => {
    expect(service.sum(2,3)).toEqual(5);
  });
});
```

Ao salvar o arquivo, o *Karma* deve realizar automaticamente os testes mais uma vez.

## Criando alguns testes

Aqui utilizamos o `TestBed`:

```{.ts .r-stretch .numberLines}
  it('sum(2,3) should return 5', () => {
    expect(service.sum(2,3)).toEqual(5);
  });
```

Entretanto, para testes simples, ele não é necessário:

```{.ts .r-stretch .numberLines}
  it('sum(3,6) should return 9', () => {
    const sumService = new SumService();
    expect(sumService.sum(3,6)).toEqual(9);
  })
```

## Prática

1. Implemente o serviço de soma apresentado e teste-o;
2. Implemente o serviço de subtração e teste-o;

## Dependency Injections por toda parte

- O `SumService`, além de pequeno, é bastante simples pois **não possui nenhuma dependência**.
- O Angular se baseia fortemente em Injeção de Dependência (*Dependency Injection*).
- Por essa razão, nossos testes precisam se adequar a essa realidade.

## Aumentando a Aplicação Calculadora

Para simular um teste com dependência, vamos aumentar nossa aplicação:

```bash
$ ng g service multiplyBySum
CREATE src/app/multiply-by-sum.service.spec.ts (394 bytes)
CREATE src/app/multiply-by-sum.service.ts (142 bytes)
```

Esse serviço irá realizar multiplicações através de sucessivas somas (`SumService`).

## `MultiplyBySumService`

```{.ts .r-stretch .numberLines}
import { Injectable } from '@angular/core';
import { SumService } from './sum.service';

@Injectable({
  providedIn: 'root'
})
export class MultiplyBySumService {

  constructor(private sumService: SumService) { }

  multiply(num1: number, num2: number): number {
    let result = 0;

    for (let i = 0; i < num2; i++) {
      result = this.sumService.sum(result, num1);        
    }

    return result;
  }
}
```

Lembre-se de adicionar `MultiplyBySumService` como um `provider` em `app.module.ts`.

## Teste *default* sem dependência

O código inicial não se preocupa com a dependência:

```{.ts .r-stretch .numberLines}
import { TestBed } from '@angular/core/testing';

import { MultiplyBySumService } from './multiply-by-sum.service';

describe('MultiplyBySumService', () => {
  let service: MultiplyBySumService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MultiplyBySumService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
```

Para testar o serviço `MultiplyBySumService`, precisamos configurar a dependência do *service*.

## Dependências com TestBed

Para isso, utilizaremos a funcionalidade `SpyObj` do *Jasmine*. É semelhante ao *Mock* do *Jest*:

```{.ts .r-stretch .numberLines data-line-numbers="1-26|3|7-8|10-21" }
import { TestBed } from '@angular/core/testing';

import { SumService } from './sum.service';
import { MultiplyBySumService } from './multiply-by-sum.service';

describe('MultiplierBySumService', () => {
  let sumServiceSpy: jasmine.SpyObj<SumService>;
  let multiplyBySumService: MultiplyBySumService;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('CalculatorService', ['sum']);

    TestBed.configureTestingModule({
      providers:[
        MultiplyBySumService,
        { provide: SumService, useValue: spy }
      ]
    });
    sumServiceSpy = TestBed.inject(SumService) as jasmine.SpyObj<SumService>;
    multiplyBySumService = TestBed.inject(MultiplyBySumService);
  });

  it('should be created', () => {
    expect(multiplyBySumService).toBeTruthy();
  });
});
```

## Dependências com TestBed

Agora com o *TestBed* e o *Spy* configurados, podemos testar o serviço.

```{.ts .r-stretch .numberLines data-line-numbers="1-4|2" }
  it('multiply(2,3) should return 6', () => {
    sumServiceSpy.sum.and.returnValues(2,4,6);
    expect(multiplyBySumService.multiply(2,3)).toEqual(6);
  });
```

- Aqui, utilizamos uma implementação falsa para o método `sum`.
- Sabemos que devem ser realizadas 3 chamadas de `sum` e os retornos devem ser 2, 4 e 6 respectivamente.
- A sintaxe do *Jasmine* `.and.returnValues(...)` permite simular esse comportamento.

## Expect para Spy

Podemos ainda verificar se o método `sum` foi chamado o número correto de vezes:

```{.ts .numberLines}
    expect(sumServiceSpy.sum.calls.count())
      .toBe(3, 'spy method was called three times');
```

## Prática

1. Implemente o serviço `MultiplyBySumService` apresentado e teste-o:
  1. Adicione um teste para `multiply(3,2)`;                                     
2. Implemente o serviço de potenciação por multiplicação (`PowerByMultiplyService`) e teste-o;

## Testando services Reativos

- *Services* reativos, por vezes, retornam objetos do tipo `Observable`;
  - Um exemplo clássico é o teste para requisições HTTP;
  - Como sabemos, nosso teste precisa ser independente e executável em qualquer ambiente;
  - Ou seja, precisamos simular a requisição e sua resposta;
- Para testar situações como essa, precisamos prepara nossos testes para retornos assíncronos.

## Exemplo Tour of Heroes

- Para esse exemplo, utilizaremos a aplicação indicada no guia do Angular, chamada **Tour of Heroes**:
  - [https://angular.io/generated/zips/toh-pt6/toh-pt6.zip](https://angular.io/generated/zips/toh-pt6/toh-pt6.zip)
- Essa aplicação simula o acesso a um web service em `HeroService`.

## `HeroService`

```{.ts .numberLines .r-stretch}
// ...
@Injectable({ providedIn: 'root' })
export class HeroService {

  private heroesUrl = 'api/heroes';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET heroes from the server */
  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.heroesUrl)
      .pipe(
        tap(_ => this.log('fetched heroes')),
        catchError(this.handleError<Hero[]>('getHeroes', []))
      );
  }
  // ...
}
```

## Exemplo de teste para `HeroService`

```{.ts .numberLines }
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';

import { Hero } from './hero';
import { HeroService } from './hero.service';

describe('Hero service', () => {
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let heroService: HeroService;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({
      providers:[
        HeroService,
        { provide: HttpClient, useValue: spy }
      ]
    });
    httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
    heroService = TestBed.inject(HeroService);

  });

  it('should return expected heroes (HttpClient called once)', (done: DoneFn) => {
    const expectedHeroes: Hero[] =
      [{ id: 1, name: 'A' }, { id: 2, name: 'B' }];

    httpClientSpy.get.and.returnValue(of(expectedHeroes));

    heroService.getHeroes().subscribe(
      heroes => {
        expect(heroes).toEqual(expectedHeroes, 'expected heroes');
        done();
      },
      done.fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

});
```

# Testando Componentes

## O que testar?

- *Components* são responsáveis pela combinação entre um *Template HTML* e uma *Classe Typescript*;
- Assim, é possível testar cada um desses itens em separado além da sua relação.

## De volta à Aplicação Calculadora

Criaremos um *component* para testarmos:

```sh
$ ng g component calculator
CREATE src/app/calculator/calculator.component.css (0 bytes)
CREATE src/app/calculator/calculator.component.html (25 bytes)
CREATE src/app/calculator/calculator.component.spec.ts (654 bytes)
CREATE src/app/calculator/calculator.component.ts (291 bytes)
UPDATE src/app/app.module.ts (552 bytes)
```

Este componente irá realizar as 3 operações (soma, multiplicação e potência) para dois números entrados pelo usuário:

![Aplicação Calculadora](./img/calculator.png)

## Teste Default

Mais uma vez, o Angular deixou testes prontos para iniciarmos:

```{.ts .numberLines .r-stretch }
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorComponent } from './calculator.component';

describe('CalculatorComponent', () => {
  let component: CalculatorComponent;
  let fixture: ComponentFixture<CalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalculatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
```

## Template HTML (calculator.component.html)

```{.html .numberLines }
<form>
  Número 1: <input id="num1" #num1 type="text" /><br />
  Número 2: <input id="num2" #num2 type="text" /><br />
  <input id="calc" type="button" value="Calcular"
    (click)="calculate(num1.value,num2.value)">
</form>

<div class="results">
  Resultado soma: <input type="text" id="resultSum" [value]="sumResult" readonly><br />
  Resultado multiplicação: <input type="text" id="resultMultiply" [value]="multResult" readonly><br />
  Resultado potência: <input type="text" id="resultPower" [value]="powResult" readonly><br />
</div>
```

## Template HTML (app.component.html)

```{.html .numberLines }
<app-calculator></app-calculator>
```

## Classe do component (calculator.component.ts)

```{.ts .numberLines }
import { Component, OnInit } from '@angular/core';

import { SumService } from '../sum.service';
import { MultiplyBySumService } from '../multiply-by-sum.service';
import { PowerByMultiplyService } from '../power-by-multiply.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  private sumRes: number = 0;
  private multRes: number = 0;
  private powRes: number = 0;

  constructor(
    private sumService: SumService,
    private multiplyBySumService: MultiplyBySumService,
    private powerByMultiplyService: PowerByMultiplyService
  ) { }

  ngOnInit(): void {
  }

  calculate(num1String: string, num2String: string): void {
    let num1: number = parseInt(num1String);
    let num2: number = parseInt(num2String);

    this.sum(num1,num2);
    this.multiply(num1,num2);
    this.power(num1,num2);
  }

  sum(num1: number, num2: number) {
    this.sumRes = this.sumService.sum(num1,num2);
  }

  multiply(num1: number, num2: number) {
    this.multRes = this.multiplyBySumService.multiply(num1,num2);
  }

  power(num1: number, num2: number) {
    this.powRes = this.powerByMultiplyService.power(num1,num2);
  }

  get sumResult() {
    return this.sumRes;
  }

  get multResult() {
    return this.multRes;
  }

  get powResult() {
    return this.powRes;
  }
}
```

## Exemplos de Testes para Component

```{.ts .numberLines }
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SumService } from '../sum.service';
import { MultiplyBySumService } from '../multiply-by-sum.service';
import { PowerByMultiplyService } from '../power-by-multiply.service';

import { CalculatorComponent } from './calculator.component';

describe('CalculatorComponent', () => {
  let component: CalculatorComponent;
  let fixture: ComponentFixture<CalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalculatorComponent ],
      providers: [
        //SumService,
        //MultiplyBySumService, 
        //PowerByMultiplyService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a form', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('form')).toBeTruthy();
  });
 
  it('should present results 5, 6 and 8 after calculate(2,3)', () => {
    component.calculate('2','3');
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    const resultSumInput:HTMLInputElement = compiled.querySelector('#resultSum')!;
    const resultMultiplyInput:HTMLInputElement = compiled.querySelector('#resultMultiply')!;
    const resultPowerInput:HTMLInputElement = compiled.querySelector('#resultPower')!;
    expect(resultSumInput.value).toBe('5');
    expect(resultMultiplyInput.value).toBe('6');
    expect(resultPowerInput.value).toBe('8');
  });
  
  it('should present results 5, 6 and 8 after user enter values 2 and 3 then click the button', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    const num1Input: HTMLInputElement = compiled.querySelector('#num1')!;
    const num2Input: HTMLInputElement = compiled.querySelector('#num2')!;
    const calcButton: HTMLInputElement = compiled.querySelector('#calc')!;
    num1Input.value = '2';
    num2Input.value = '3';
    num1Input.dispatchEvent(new Event('input'));
    num2Input.dispatchEvent(new Event('input'));
    calcButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    const resultSumInput:HTMLInputElement = compiled.querySelector('#resultSum')!;
    const resultMultiplyInput:HTMLInputElement = compiled.querySelector('#resultMultiply')!;
    const resultPowerInput:HTMLInputElement = compiled.querySelector('#resultPower')!;
    expect(resultSumInput.value).toBe('5');
    expect(resultMultiplyInput.value).toBe('6');
    expect(resultPowerInput.value).toBe('8');
  });

});
```

## Mais informações

- [Cenários de Testes de Componentes](https://angular.io/guide/testing-components-scenarios);
- [Utilitários da API Angular para Testes](https://angular.io/guide/testing-utility-apis#testing-utility-apis).

## Referências
- Angular Docs <https://angular.io>. Acessado em: 27/08/21.
