% Clean Code: funções e métodos
% Alex Kutzke
% Agosto 2023

# Funções e Métodos

## Consegue entender essa função?

Exemplo 1

```{.java .numberLines}
public static String testableHtml(
    PageData pageData,
    boolean includeSuiteSetup
    ) throws Exception {
  WikiPage wikiPage = pageData.getWikiPage();
  StringBuffer buffer = new StringBuffer();
  if (pageData.hasAttribute("Test")) {
    if (includeSuiteSetup) {
      WikiPage suiteSetup =
        PageCrawlerImpl.getInheritedPage(
            SuiteResponder.SUITE_SETUP_NAME, wikiPage
            );
      if (suiteSetup != null) {
        WikiPagePath pagePath =
          suiteSetup.getPageCrawler().getFullPath(suiteSetup);
        String pagePathName = PathParser.render(pagePath);
        buffer.append("!include -setup .")
          .append(pagePathName)
          .append("\n");
      }
    }
    WikiPage setup = 
      PageCrawlerImpl.getInheritedPage("SetUp", wikiPage);
    if (setup != null) {
      WikiPagePath setupPath =
        wikiPage.getPageCrawler().getFullPath(setup);
      String setupPathName = PathParser.render(setupPath);
      buffer.append("!include -setup .")
        .append(setupPathName)
        .append("\n");
    }
  }
  buffer.append(pageData.getContent());
  if (pageData.hasAttribute("Test")) {
    WikiPage teardown = 
      PageCrawlerImpl.getInheritedPage("TearDown", wikiPage);
    if (teardown != null) {
      WikiPagePath tearDownPath =
        wikiPage.getPageCrawler().getFullPath(teardown);
      String tearDownPathName = PathParser.render(tearDownPath);
      buffer.append("\n")
        .append("!include -teardown .")
        .append("tearDownPathName")
        .append("\n");
    }
    if (includeSuiteSetup) {
      WikiPage suiteTeardown =
        PageCrawlerImpl.getInheritedPage(
            SuiteResponder.SUITE_TEARDOWN_NAME,
            wikiPage
            );
      if (suiteTeardown != null) {
        WikiPagePath pagePath =
          suiteTeardown.getPageCrawler().getFullPath (suiteTeardown);
        String pagePathName = PathParser.render(pagePath);
        buffer.append("!include -teardown .")
          .append(pagePathName)
          .append("\n");
      }
    }
  }
  pageData.setContent(buffer.toString());
  return pageData.getHtml();
}
```

## E agora?

```{.java .numberLines .r-stretch}
public static String renderPageWithSetupsAndTeardowns(
    PageData pageData, boolean isSuite
    ) throws Exception {
  boolean isTestPage = pageData.hasAttribute("Test");
  if (isTestPage) {
    WikiPage testPage = pageData.getWikiPage();
    StringBuffer newPageContent = new StringBuffer();
    includeSetupPages(testPage, newPageContent, isSuite);
    newPageContent.append(pageData.getContent());
    includeTeardownPages(testPage, newPageContent, isSuite);
    pageData.setContent(newPageContent.toString());
  }
  return pageData.getHtml();
}
```

. . . 

Melhor? Por quê?

# Funções Pequenas!

## Regras 

::: {.incremental}
1. Funções devem ser pequenas;
2. Funções devem ser ainda menores;
:::

## Ok! Mas quão pequenas?

- Difícil dizer;
- Mas de **2 a 4 linhas** é um bom objetivo;
- Definitivamente menor do que o exemplo 1 refatorado.

## Exemplo 1 re-refatorado

```{.java .numberLines .r-stretch}
public static String renderPageWithSetupsAndTeardowns(
    PageData pageData, boolean isSuite) throws Exception {
  if (isTestPage(pageData))
    includeSetupAndTeardownPages(pageData, isSuite);
  return pageData.getHtml();
}
```

## Blocos e Indentação

- Isso quer dizer que blocos de comandos `if`, `else`, `while`, etc., devem ter uma única linha;
- Provavelmente, essa linha deve ser a chamada de um outra função;
- Mantem funções pequenas;
- Com bons nomes, torna-se uma ótima documentação.
- Além disso, o nível de aninhamento devem estar entre 1 e 2.

# Faça apenas uma coisa

## Faça apenas uma coisa

- O exemplo 1 faz várias coisas;
- Por outro lado, o exemplo 1 re-refatorado, faz apenas uma coisa:
  - Inclui `setups` e `teardowns` em páginas teste;
- Por isso:

> Funções devem fazer uma coisa. Devem fazer ela bem. E devem fazer apenas ela.

## Mas quanto é "uma coisa"?

```{.java .numberLines .r-stretch}
public static String renderPageWithSetupsAndTeardowns(
    PageData pageData, boolean isSuite) throws Exception {
  if (isTestPage(pageData))
    includeSetupAndTeardownPages(pageData, isSuite);
  return pageData.getHtml();
}
```

Isso é apenas uma coisa?

. . . 

1. Determina se é uma página teste;
2. Se sim, inclui `setups` e `teardowns`;
3. Renderiza a página com HTML.

## E agora?

Ela faz 1 ou 3 coisas?

> TO RenderPageWithSetupsAndTeardowns, we check to see whether the page is a test page and if so, we include the setups and teardowns. In either case we render the page in HTML.

O segredo está no **nível de abstração**.

# Um nível de abstração por função

## Nível de abstração

- Nível de abstração?
  - Código baixo nível = baixa abstração;
  -  Código alto nível = alta abstração.

## Um único nível de abstração

- Para fazer apenas uma coisa, uma função precisa:
  - Que **todos** os seus comandos estejam em **um mesmo nível de abstração**;
  - E que estejam um único nível abaixo do **nome da função**.

## Exemplo 1 novamente

```{.java .numberLines .r-stretch data-line-numbers="17,5|9|11-12"}
public static String testableHtml(
    PageData pageData,
    boolean includeSuiteSetup
    ) throws Exception {
  WikiPage wikiPage = pageData.getWikiPage();

  // ...

        String pagePathName = PathParser.render(pagePath);
        buffer.append("!include -setup .")
          .append(pagePathName)
          .append("\n");
      }
    }
  }
  pageData.setContent(buffer.toString());
  return pageData.getHtml();
}
```

## Por quê?

- Misturar nível de abstração confunde o leitor;
- Difícil diferenciar conceitos essenciais de meros detalhes;

## Leitura top-down

Queremos ler o código de forma "top-down".

Uma sequência lógica, de cima para baixo, de declarações.

**Como um texto**.

## Use nomes descritivos

- Nomes importam;
- Não há problemas se forem longos:
  - Melhor um nome longo descritivo do que um curto enigmático;
- Dedique tempo a escolha do nome;
- Teste várias opções;
- Seja consistente nos formatos e escolhas de palavras;
- Um bom nome de função auxilia na escrita e na leitura do código;

## Essa função pode ser dividida em outras 3. Quais?

```{.js .numberLines }
function login(email, password) {
  if (!email.includes('@') ||password.length < 7) {
    thrownewError('Invalid input!');  
  }

  const existingUser = database.find('users', 'email', '==', email);

  if (!existingUser) {
    thrownewError('Could not find a user for the provided email.');
  }

  if (existingUser.password===password) {
    // create a session  
  } 
  else {
    thrownewError('Invalid credentials!');  
  }
}
```

## Uma opção


```{.js .numberLines }
function login(email, password) {
  validateUserInput(email, password);

  const existingUser = findUserByEmail(email);

  existingUser.validatePassword(password);
}
```

## E uma outra


```{.js .numberLines }
function login(email, password) {
  if (inputInvalid(email, password)) {
    showError(email, password);
    return;  
  }

  verifyCredentials(email, password);
  createSession();
}
```

## Duas regras simples

1. Extraia código que realiza uma mesma funcionalidade ou que é relacionado;
2. Extraia código que requer mais interpretação do que o código em sua volta.

## Exemplo regra 1

```{.js .numberLines }
function updateUser(userData) {
  validateUserData(userData);
  const user = findUserById(userData.id);
  user.setAge(userData.age);
  user.setName(userData.name);
  user.save();
}
```

## Exemplo regra 1 refatorado

```{.js .numberLines }
function updateUser(userData) {
  validateUserData(userData);
  applyUpdate(userData);
}

function applyUpdate(userData) {
  const user = findUserById(userData.id);
  user.setAge(userData.age);
  user.setName(userData.name);
  user.save();
}
```

## Exemplo regra 2 

```{.js .numberLines }
function processTransaction(transaction) {
  if (transaction.type === 'UNKNOWN') {
    thrownewError('Invalid transaction type.');  
  }

  if (transaction.type === 'PAYMENT') {
    processPayment(transaction);  
  }
}
```

## Exemplo regra 2 refatorado

```{.js .numberLines }
function processTransaction(transaction) {
  validateTransaction(transaction);
  if (isPayment(transaction)) {
    processPayment(transaction);  
  }
}
```

## Quando não extrair uma nova função?

3 **sinais**:

1. Se você acaba apenas **renomeando uma operação**;
2. Se você tem que rolar a tela várias vezes para acompanhar a leitura ou a ideia de uma função simples;
3. Você tem dificuldade para renomear a função extraída sem utilizar um nome já em uso.

## Quando não extrair uma nova função?

```{.java .numberLines }
function throwError(message) {
  thrownewError(message);
}

function saveUser(email, password) {
  const user = buildUser(email, password);
  user.save();
}

function buildUser(email, password) {
  return newUser(email, password);
}
```

# Argumentos

## Quantos argumentos?

- Nenhum: ideal!;
- Um: legal;
- Dois: começa a complicar, muitas combinações;
- Três: ainda mais complexo, evitar sempre que possível;
- Mais de três: reescrever. :)

- Quanto mais argumentos, mais difícil de testar.

## Funções com um único argumento

- Duas formas comuns:
  - Faz uma **pergunta** sobre o argumento:

```{.java .numberLines }
boolean fileExists("MyFile");
```

  - Opera sobre o argumento, o transforma em outra coisa e a **retorna**:   

```{.java .numberLines }
InputStream fileOpen("MyFile")
```
Transforma uma `String` em um `InputStream`.

## Argumento Booleano (Flag)


```{.java .numberLines }
render(true);
```

- Não utilize;
- A função claramente faz mais de uma coisa (uma para true e outra para false);
- Dividir a função em duas com nomes claros.

## Funções com Dois argumentos

- Mais difícil de entender do que apenas um argumento;

```{.java .numberLines }
writeField(name);
// ou?
writeField(output-stream, name);
```

- Acabamos por ignorar o primeiro argumento (no caso acima);
- Bugs tendem a se esconder nos trechos que ignoramos.

## Funções com Dois argumentos

- Por vezes, são perfeitamente aceitáveis:

```{.java .numberLines }
Point p = new Point(0,0);
```

Porém é importante lembrar que dois ou mais argumentos exigem uma **ordem**. E nem
sempre essa ordem será "natural".

Classes e métodos, geralmente, permitem a conversão de funções de dois argumentos em estruturas de um único argumento.

## Como melhorar essa função?


```{.js .numberLines }
function log(message, isError) {
  if (isError) {
    console.error(message);
  } else {
    console.log(message);
  }
}

log('Hi there!', false);
```

## Funções com Três argumentos

- Evitar ao máximo;
- Fácil errar a ordem dos argumentos;
- Difícil encontrar bons nomes.

## Linguagens podem ajudar


```{.js .numberLines }
class User {
  constructor(name, age, email) {
    this.name = name;
    this.age = age;
    this.email = email;
  }
}

const user = new User('Max', 31, 'max@test.com');

class User {
  constructor(userData) {
    this.name = userData.name;
    this.age = userData.age;
    this.email = userData.email;
  }
}

const user = new User({ name: 'Max', email: 'max@test.com', age: 31 });
```

# Sem efeitos colaterais

## Efeito colateral

Efeito colateral de uma função é quando esta faz o que é esperado mas faz também outras ações (geralmente inesperadas).

Ou seja, de alguma forma, o **estado** da aplicação é alterado pela função.

## Exemplo

Qual o efeito colateral aqui?

```{.java .numberLines data-line-numbers="1-15|9" }
public class UserValidator {
  private Cryptographer cryptographer;
  public boolean checkPassword(String userName, String password) {
    User user = UserGateway.findByName(userName);
    if (user != User.NULL) {
      String codedPhrase = user.getPhraseEncodedByPassword();
      String phrase = cryptographer.decrypt(codedPhrase, password);
      if ("Valid Password".equals(phrase)) {
        Session.initialize();
        return true;
      }
    }
    return false;
  }
}
```

## Argumentos de saída

Argumentos com valores alterados pela função.

```{.java .numberLines }
appendFooter(s);
```

Exige olhar a declaração da função:

```{.java .numberLines }
public void appendFooter(StringBuffer report)
```

- Olhar a declaração é "double-check";
- Orientação a objetos ajudam a evitar argumentos de saída:

```{.java .numberLines }
report.appendFooter();
```

# Don't Repeat Yourself (DRY)

## Raiz de todo mal 

- Duplicação pode ser a raiz de todo mal (depois da otimização prematura);
- Código duplicado (ou seja, trechos que fazem a mesma operação) exigem maior esforço para serem mantidos:
  - Levando a um maior esforço para evitar erros;
  - E um maior esforço para testar;
- Refatorar sempre!

## Prática - Limpar o código abaixo

```{.js .numberLines .r-stretch }
function createUser(email, password) {
  if (!email || !email.includes('@') || !password || password.trim() === '') {
    console.log('Invalid input!');
    return;
  }
  
  const user = {
    email: email,
    password = password,
  };

  database.insert(user);
}
```

## Referências

- Martin, Robert C. Clean code: a handbook of agile software craftsmanship. Pearson Education, 2009.
- Schwarzmüller, Maximilian. Clean code course. Academind, 2021.
